<?php
/**
* Cash On Delivery With Fee
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2017 idnovate
*  @license   See above
*/

class AdminCodfeeConfigurationController extends ModuleAdminController
{
    protected $delete_mode;
    protected $_defaultOrderBy = 'priority';
    protected $_defaultOrderWay = 'ASC';
    protected $can_add_codfeeconf = true;
    protected $top_elements_in_list = 4;

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'codfee_configuration';
        $this->className = 'CodfeeConfiguration';
        $this->tabClassName = 'AdminCodfeeConfiguration';
        $this->module_name = 'codfee';
        $this->lang = true;
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        $this->_orderWay = $this->_defaultOrderWay;
        $this->show_toolbar = true;
        $this->allow_export = true;
        $this->imageType = 'png';

        parent::__construct();

        if (version_compare(_PS_VERSION_, '1.6', '>=')) {
            $this->meta_title[] = $this->l('Cash on delivery with fee configuration');
        } else {
            $this->meta_title = $this->l('Cash on delivery with fee configuration');
        }
        $this->tpl_list_vars['title'] = $this->l('List of cash on delivery fee configurations');
        $this->taxes_included = (Configuration::get('PS_TAX') == '0' ? false : true);

        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash'
            )
        );

        $this->context = Context::getContext();

        $this->default_form_language = $this->context->language->id;

        $this->fieldImageSettings = array(
            'name' => 'logo',
            'dir' => 'tmp',
        );

        $this->fields_list = array(
            'id_codfee_configuration' => array(
                'title' => $this->l('ID'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs'
            ),
            'name' => array(
                'title' => $this->l('Name')
            ),
            'logo' => array(
                'title' => $this->l('Payment image'),
                'image' => 'tmp',
                'orderby' => false,
                'search' => false,
                'align' => 'center',
            ),
            'active' => array(
                'title' => $this->l('Enabled'),
                'align' => 'text-center',
                'active' => 'status',
                'type' => 'bool',
                'callback' => 'printActiveIcon'
            ),
            'type' => array(
                'title' => $this->l('Type'),
                'callback' => 'getCodfeeType',
                'align' => 'text-center'
            ),
            'fix' => array(
                'title' => $this->l('Fix'),
                'align' => 'text-center'
            ),
            'percentage' => array(
                'title' => $this->l('Percentage'),
                'align' => 'text-center'
            ),
            /*
            'min' => array(
                'title' => $this->l('Minimum fee'),
                'align' => 'text-center'
            ),
            'max' => array(
                'title' => $this->l('Maximum fee'),
                'align' => 'text-center'
            ),
            */
            'groups' => array(
                'title' => $this->l('Group(s)'),
                'callback' => 'getCustomerGroups',
                'align' => 'text-center'
            ),
            'carriers' => array(
                'title' => $this->l('Carrier(s)'),
                'callback' => 'getCarriers',
                'align' => 'text-center'
            ),
            'countries' => array(
                'title' => $this->l('Country(s)'),
                'callback' => 'getCountries',
                'align' => 'text-center'
            ),
            'zones' => array(
                'title' => $this->l('Zone(s)'),
                'callback' => 'getZones',
                'align' => 'text-center'
            ),
            'categories' => array(
                'title' => $this->l('Category(s)'),
                'callback' => 'getCategories',
                'align' => 'text-center'
            ),
            'show_conf_page' => array(
                'title' => $this->l('Show conf page'),
                'align' => 'text-center',
                'type' => 'bool',
                'callback' => 'printShowConfPageIcon',
                'filter_key' => 'a!show_conf_page'
            ),
            'free_on_freeshipping' => array(
                'title' => $this->l('Free on free shipping'),
                'align' => 'text-center',
                'type' => 'bool',
                'callback' => 'printFreeOnFreeShippingIcon',
                'filter_key' => 'a!free_on_freeshipping'
            ),
            'hide_first_order' => array(
                'title' => $this->l('Hide on first order'),
                'align' => 'text-center',
                'type' => 'bool',
                'callback' => 'printHideFirstOrderIcon',
                'filter_key' => 'a!hide_first_order'
            ),
            'priority' => array(
                'title' => $this->l('Priority'),
                'align' => 'text-center'
            ),
        );

        if (version_compare(_PS_VERSION_, '1.6', '<')) {
            unset($this->fields_list['show_conf_page']);
        }

        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            unset($this->fields_list['logo']);
        }

        $this->shopLinkType = 'shop';

        if (!$this->module->active) {
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminHome'));
        }

        if (Shop::isFeatureActive() && (Shop::getContext() == Shop::CONTEXT_ALL || Shop::getContext() == Shop::CONTEXT_GROUP)) {
            $this->can_add_codfeeconf = false;
        }
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addJqueryPlugin(array('typewatch', 'fancybox', 'autocomplete'));
    }

    public function initContent()
    {
        if ($this->action == 'select_delete') {
            $this->context->smarty->assign(array(
                'delete_form' => true,
                'url_delete' => htmlentities($_SERVER['REQUEST_URI']),
                'boxes' => $this->boxes,
            ));
        }
        if (!$this->can_add_codfeeconf && !$this->display) {
            $this->informations[] = $this->l('You have to select a shop if you want to create a new fee.');
        }
        parent::initContent();
    }

    public function init()
    {
        parent::init();
        parent::initBreadcrumbs(Tab::getIdFromClassName('CodfeeConfiguration'));
    }

    public function initToolbar()
    {
        parent::initToolbar();

        if (!$this->can_add_codfeeconf) {
            unset($this->toolbar_btn['new']);
        }
    }

    public function getList($id_lang, $orderBy = null, $orderWay = null, $start = 0, $limit = null, $id_lang_shop = null)
    {
        parent::getList($id_lang, $orderBy, $orderWay, $start, $limit, $id_lang_shop);
    }


    public function initToolbarTitle()
    {
        parent::initToolbarTitle();
        switch ($this->display) {
            case '':
            case 'list':
                array_pop($this->toolbar_title);
                $this->toolbar_title[] = $this->l('Manage Cash On Delivery With Fee Configuration');
                break;
            case 'view':
                if (($codfeeconf = $this->loadObject(true)) && Validate::isLoadedObject($codfeeconf)) {
                    array_pop($this->toolbar_title);
                    $this->toolbar_title[] = sprintf($this->l('Fee configuration: %s'), $codfeeconf->id_codfee_configuration.' - '.$codfeeconf->name);
                }
                break;
            case 'add':
            case 'edit':
                array_pop($this->toolbar_title);
                if (($codfeeconf = $this->loadObject(true)) && Validate::isLoadedObject($codfeeconf)) {
                    $this->toolbar_title[] = sprintf($this->l('Editing fee configuration: %s'), $codfeeconf->id_codfee_configuration.' - '.$codfeeconf->name);
                } else {
                    $this->toolbar_title[] = $this->l('Creating a new cash on delivery fee configuration');
                }
                break;
        }
    }

    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();

        if (empty($this->display)) {
            $this->page_header_toolbar_btn['desc-module-back'] = array(
                'href' => 'index.php?controller=AdminModules&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back'),
                'icon' => 'process-icon-back'
            );
            $this->page_header_toolbar_btn['desc-module-new'] = array(
                'href' => 'index.php?controller='.$this->tabClassName.'&add'.$this->table.'&token='.Tools::getAdminTokenLite($this->tabClassName),
                'desc' => $this->l('New'),
                'icon' => 'process-icon-new'
            );
            $this->page_header_toolbar_btn['desc-module-reload'] = array(
                'href' => 'index.php?controller='.$this->tabClassName.'&token='.Tools::getAdminTokenLite($this->tabClassName).'&reload=1',
                'desc' => $this->l('Reload'),
                'icon' => 'process-icon-refresh'
            );
            $this->page_header_toolbar_btn['desc-module-translate'] = array(
                'href' => '#',
                'desc' => $this->l('Translate'),
                'modal_target' => '#moduleTradLangSelect',
                'icon' => 'process-icon-flag'
            );
            $this->page_header_toolbar_btn['desc-module-hook'] = array(
                'href' => 'index.php?tab=AdminModulesPositions&token='.Tools::getAdminTokenLite('AdminModulesPositions').'&show_modules='.Module::getModuleIdByName('codfee'),
                'desc' => $this->l('Manage hooks'),
                'icon' => 'process-icon-anchor'
            );
        }

        if (!$this->can_add_codfeeconf) {
            unset($this->page_header_toolbar_btn['desc-module-new']);
        }
    }

    public function initModal()
    {
        parent::initModal();

        $this->context->smarty->assign(array(
            'trad_link' => 'index.php?tab=AdminTranslations&token='.Tools::getAdminTokenLite('AdminTranslations').'&type=modules&module='.$this->module_name.'&lang=',
            'module_languages' => Language::getLanguages(false),
            'module_name' => 'codfee',
        ));

        $modal_content = $this->context->smarty->fetch('controllers/modules/modal_translation.tpl');
        $this->modals[] = array(
            'modal_id' => 'moduleTradLangSelect',
            'modal_class' => 'modal-sm',
            'modal_title' => $this->l('Translate this module'),
            'modal_content' => $modal_content
        );
    }

    public function initProcess()
    {
        parent::initProcess();

        if (Tools::getIsset('reload')) {
            $this->action = 'reset_filters';
        }

        if (Tools::isSubmit('changeActiveVal') && $this->id_object) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_active_val';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('changeShowConfPageVal') && $this->id_object) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_show_conf_page_val';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('changeFreeOnFreeShippingVal') && $this->id_object) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_free_on_freeshipping_val';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('changeHideFirstOrderVal') && $this->id_object) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_hide_first_order_val';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }
    }

    public function renderList()
    {
        if ((Tools::isSubmit('submitBulkdelete'.$this->table) || Tools::isSubmit('delete'.$this->table)) && $this->tabAccess['delete'] === '1') {
            $this->tpl_list_vars = array(
                'delete_codfeeconf' => true,
                'REQUEST_URI' => $_SERVER['REQUEST_URI'],
                'POST' => $_POST
            );
        }
        return parent::renderList();
    }

    public function renderOptions()
    {
        return parent::renderOptions();
    }

    public function renderForm()
    {
        if (!($codfeeconf = $this->loadObject(true))) {
            return;
        }

        $image = _PS_TMP_IMG_DIR_.$codfeeconf->id.'.'.$this->imageType;
        $image_url = ImageManager::thumbnail($image, $this->module_name.'_'.(int)$codfeeconf->id.'.'.$this->imageType, 350, $this->imageType, true, true);
        $image_size = file_exists($image) ? filesize($image) / 1000 : false;

        //Get fee types
        $types = $this->getCodfeeTypes();

        //Get carrier, customer groups and order statuses
        $groups = array(array('id_group' => 'all', 'name' => $this->l('-- All --')));
        $carriers = array(array('id_reference' => 'all', 'name' => $this->l('-- All --')));
        $zones = array(array('id_zone' => 'all', 'name' => $this->l('-- All --')));
        $countries = array(array('id_country' => 'all', 'name' => $this->l('-- All --')));
        $categories = array(array('id_category' => 'all', 'name' => $this->l('-- All --')));
        $manufacturers = array(array('id_manufacturer' => 'all', 'name' => $this->l('-- All --')));
        $suppliers = array(array('id_supplier' => 'all', 'name' => $this->l('-- All --')));
        if (version_compare(_PS_VERSION_, '1.5', '<')) {
            $carriers = array_merge($carriers, Carrier::getCarriers($this->context->cookie->id_lang, true, false, false, null, PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE));
            $statuses = OrderState::getOrderStates((int)$this->context->cookie->id_lang);
            $groups = array_merge($groups, Group::getGroups($this->context->cookie->id_lang, true));
            $categories = array_merge($categories, Category::getCategories((int)($this->context->cookie->id_lang), false, false));
            $countries = array_merge($countries, Country::getCountries((int)($this->context->cookie->id_lang)));
            $manufacturers = array_merge($manufacturers, Manufacturer::getManufacturers(false, (int)($this->context->cookie->id_lang), false));
            $suppliers = array_merge($suppliers, Supplier::getSuppliers(false, (int)($this->context->cookie->id_lang), false));
        } else {
            $carriers = array_merge($carriers, Carrier::getCarriers($this->context->language->id, true, false, false, null, Carrier::ALL_CARRIERS));
            $statuses = OrderState::getOrderStates((int)$this->context->language->id);
            $groups = array_merge($groups, Group::getGroups($this->context->language->id, true));
            $categories = array_merge($categories, Category::getCategories((int)($this->context->language->id), false, false));
            $countries = array_merge($countries, Country::getCountries((int)($this->context->language->id)));
            $manufacturers = array_merge($manufacturers, Manufacturer::getManufacturers(false, (int)($this->context->language->id), false));
            $suppliers = array_merge($suppliers, Supplier::getSuppliers(false, (int)($this->context->language->id), false));
        }

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Cash On Delivery Fee Configuration'),
                'icon' => 'icon-key'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Name'),
                    'name' => 'name',
                    'required' => true,
                    'col' => '3',
                    'hint' => $this->l('Invalid characters:').' !&lt;&gt;,;?=+()@#"°{}_$%:'
                ),
                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                    'label' => $this->l('Active'),
                    'name' => 'active',
                    'class' => 't',
                    'col' => '1',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                    'hint' => $this->l('Enable or Disable COD Fee Configuration')
                ),
                array(
                    'type' => version_compare(_PS_VERSION_, '1.7', '>=') ? 'hidden' : 'file',
                    'label' => $this->l('Payment image'),
                    'name' => 'logo',
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                    'display_image' => true,
                    'col' => '5',
                    'hint' => $this->l('Upload a payment image for your Checkout page.')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Type'),
                    'name' => 'type',
                    'required' => true,
                    'col' => '3',
                    'class' => 'fixed-width-md',
                    'options' => array(
                        'query' => $types,
                        'id' => 'id',
                        'name' => 'name',
                        'default' => array(
                            'value' => '',
                            'label' => $this->l('-- Choose --')
                        )
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Fix'),
                    'name' => 'fix',
                    'lang' => false,
                    'col' => '1',
                    'hint' => sprintf($this->l('Fix fee (by default currency) %s'), ($this->taxes_included) ? $this->l('taxes included') : $this->l('without taxes')),
                    /*'desc' => ($this->taxes_included) ? $this->l('With taxes') : $this->l('Without taxes'),*/
                    'suffix' => $this->context->currency->sign
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Percentage'),
                    'name' => 'percentage',
                    'lang' => false,
                    'col' => '1',
                    'hint' => $this->l('Percentage fee'),
                    'suffix' => '%'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Minimun fee'),
                    'name' => 'min',
                    'lang' => false,
                    'col' => '1',
                    'hint' => $this->l('Minimum fee to add (by default currency)'),
                    'suffix' => $this->context->currency->sign
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Maximum fee'),
                    'name' => 'max',
                    'lang' => false,
                    'col' => '1',
                    'hint' => $this->l('Maximum fee to add (by default currency)'),
                    'suffix' => $this->context->currency->sign
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Order minimum'),
                    'name' => 'order_min',
                    'lang' => false,
                    'col' => '1',
                    'hint' => $this->l('Order minimum amount to enable this payment option (zero to disable)'),
                    'suffix' => $this->context->currency->sign
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Order maximum'),
                    'name' => 'order_max',
                    'lang' => false,
                    'col' => '1',
                    'hint' => $this->l('Order maximum amount to enable this payment option (zero to disable)'),
                    'suffix' => $this->context->currency->sign
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Order amount free fee'),
                    'name' => 'amount_free',
                    'lang' => false,
                    'col' => '1',
                    'hint' => $this->l('Order amount for free fee (zero to disable)'),
                    'suffix' => $this->context->currency->sign
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Customer group(s)'),
                    'name' => 'groups[]',
                    'multiple' => true,
                    'required' => true,
                    'col' => '5',
                    'class' => 'fixed-width-md',
                    'options' => array(
                        'query' => $groups,
                        'id' => 'id_group',
                        'name' => 'name'
                    ),
                    'hint' => $this->l('Customer group(s) with this payment option enabled'),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Carrier(s) allowed'),
                    'name' => 'carriers[]',
                    'multiple' => true,
                    'required' => true,
                    'col' => '5',
                    'class' => 'fixed-width-md',
                    'options' => array(
                        'query' => $carriers,
                        'id' => 'id_reference',
                        'name' => 'name'
                    ),
                    'hint' => $this->l('Carrier(s) with this payment option enabled')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Country(s) allowed'),
                    'name' => 'countries[]',
                    'multiple' => true,
                    'required' => true,
                    'col' => '5',
                    'class' => 'fixed-width-md',
                    'options' => array(
                        'query' => $countries,
                        'id' => 'id_country',
                        'name' => 'name'
                    ),
                    'hint' => $this->l('Country(s) with this payment option enabled')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Zone(s) allowed'),
                    'name' => 'zones[]',
                    'multiple' => true,
                    'required' => true,
                    'col' => '5',
                    'class' => 'fixed-width-md',
                    'options' => array(
                        'query' => array_merge($zones, Zone::getZones()),
                        'id' => 'id_zone',
                        'name' => 'name'
                    ),
                    'hint' => $this->l('Zone(s) with this payment option enabled')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Category(s) allowed'),
                    'name' => 'categories[]',
                    'multiple' => true,
                    'required' => true,
                    'col' => '5',
                    'class' => 'fixed-width-md',
                    'options' => array(
                        'query' => $categories,
                        'id' => 'id_category',
                        'name' => 'name'
                    ),
                    'hint' => $this->l('Category(s) with this payment option enabled'),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Manufacturer(s) allowed'),
                    'name' => 'manufacturers[]',
                    'multiple' => true,
                    'required' => true,
                    'col' => '5',
                    'class' => 'fixed-width-md',
                    'options' => array(
                        'query' => $manufacturers,
                        'id' => 'id_manufacturer',
                        'name' => 'name'
                    ),
                    'hint' => $this->l('Manufacturer(s) with this payment option enabled')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Supplier(s) allowed'),
                    'name' => 'suppliers[]',
                    'multiple' => true,
                    'required' => true,
                    'col' => '5',
                    'class' => 'fixed-width-md',
                    'options' => array(
                        'query' => $suppliers,
                        'id' => 'id_supplier',
                        'name' => 'name'
                    ),
                    'hint' => $this->l('Supplier(s) with this payment option enabled')
                ),
                array(
                    'type' => 'textarea',
                    'label' => (version_compare(_PS_VERSION_, '1.7', '>=')) ? $this->l('Order confirmation text') : $this->l('Text in checkout'),
                    'name' => 'payment_text',
                    'lang' => true,
                    'cols' => 60,
                    'rows' => 10,
                    'autoload_rte' => 'rte',
                    'col' => 6,
                    'hint' => array(
                        $this->l('Invalid characters:').' &lt;&gt;;=#{}',
                        (version_compare(_PS_VERSION_, '1.7', '>=')) ? $this->l('Additional text to show in order confirmation page.') : $this->l('Additional text to show in checkout payment method.')
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Initial order status'),
                    'name' => 'initial_status',
                    'required' => true,
                    'col' => '3',
                    'class' => 'fixed-width-md',
                    'options' => array(
                        'query' => $statuses,
                        'id' => 'id_order_state',
                        'name' => 'name',
                        'default' => array(
                            'value' => '',
                            'label' => $this->l('-- Choose --')
                        )
                    ),
                    'hint' => $this->l('Initial status when an order is placed with this payment option')
                ),
                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                    'label' => $this->l('Show confirmation page'),
                    'name' => 'show_conf_page',
                    'class' => 't',
                    'col' => '1',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'show_conf_page_on',
                            'value' => 1,
                            'label' => $this->l('Show')
                        ),
                        array(
                            'id' => 'show_conf_page_off',
                            'value' => 0,
                            'label' => $this->l('Don\'t show')
                        )
                    ),
                    'hint' => $this->l('Show checkout confirmation page')
                ),
                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                    'label' => $this->l('Free on free shipping'),
                    'name' => 'free_on_freeshipping',
                    'class' => 't',
                    'col' => '1',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'free_on_freeshipping_on',
                            'value' => 1,
                            'label' => $this->l('Free on free shipping')
                        ),
                        array(
                            'id' => 'free_on_freeshipping_off',
                            'value' => 0,
                            'label' => $this->l('Not free on free shipping')
                        )
                    ),
                    'hint' => $this->l('Do not apply fee when order has free shipping')
                ),
                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                    'label' => $this->l('Hide on customer first order'),
                    'name' => 'hide_first_order',
                    'class' => 't',
                    'col' => '1',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'hide_first_order_on',
                            'value' => 1,
                            'label' => $this->l('Hide on first order')
                        ),
                        array(
                            'id' => 'hide_first_order_off',
                            'value' => 0,
                            'label' => $this->l('Don\'t hide on first order')
                        )
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Priority'),
                    'name' => 'priority',
                    'col' => '1',
                    'hint' => $this->l('Select priority when 2 or more fee configurations overlaps. Fee with less number here will shown')
                ),
            )
        );

        $this->fields_form['submit'] = array(
            'title' => $this->l('Save'),
        );

        if (version_compare(_PS_VERSION_, '1.6', '<')) {
            $image = ImageManager::thumbnail(_PS_TMP_IMG_DIR_.$codfeeconf->id.'.'.$this->imageType, $this->module_name.'_'.(int)$codfeeconf->id.'.'.$this->imageType, 350, $this->imageType, true, true);
            $this->fields_value = array(
                'image' => $image ? $image : false,
                'size' => $image ? filesize(_PS_TMP_IMG_DIR_.$codfeeconf->id.'.'.$this->imageType) / 1000 : false
            );
        }

        //Load db values for select inputs
        if ($codfeeconf->id) {
            $groups_db = explode(';', $codfeeconf->groups);
            $carriers_db = explode(';', $codfeeconf->carriers);
            $zones_db = explode(';', $codfeeconf->zones);
            $countries_db = explode(';', $codfeeconf->countries);
            $categories_db = explode(';', $codfeeconf->categories);
            $manufacturers_db = explode(';', $codfeeconf->manufacturers);
            $suppliers_db = explode(';', $codfeeconf->suppliers);
            $this->fields_value = array(
                'groups[]' => $groups_db,
                'carriers[]' => $carriers_db,
                'zones[]' => $zones_db,
                'countries[]' => $countries_db,
                'categories[]' => $categories_db,
                'manufacturers[]' => $manufacturers_db,
                'suppliers[]' => $suppliers_db
            );
        }

        return parent::renderForm();
    }

    public function renderView()
    {
        if (!($codfeeconf = $this->loadObject())) {
            return;
        }

        return parent::renderView();
    }

    public function processDelete()
    {
        parent::processDelete();
    }

    protected function processBulkDelete()
    {
        parent::processBulkDelete();
    }

    public function processAdd()
    {
        if (Tools::getValue('submitFormAjax')) {
            $this->redirect_after = false;
        }

        $this->_formValidations();

        $codfeeconf = new CodfeeConfiguration();
        $codfeeconf->name = Tools::getValue('name');
        $codfeeconf->type = Tools::getValue('type');
        $codfeeconf->fix = Tools::getValue('fix');
        $codfeeconf->percentage = Tools::getValue('percentage');
        $codfeeconf->min = Tools::getValue('min');
        $codfeeconf->max = Tools::getValue('max');
        $codfeeconf->order_min = Tools::getValue('order_min');
        $codfeeconf->order_max = Tools::getValue('order_max');
        $codfeeconf->amount_free = Tools::getValue('amount_free');
        $codfeeconf->groups = (is_array(Tools::getValue('groups')) ? (in_array('all', Tools::getValue('groups')) ? 'all' : implode(';', Tools::getValue('groups'))) : (Tools::getValue('groups') == '' ? 'all' : Tools::getValue('groups')));
        $codfeeconf->carriers = (is_array(Tools::getValue('carriers')) ? (in_array('all', Tools::getValue('carriers')) ? 'all' : implode(';', Tools::getValue('carriers'))) : (Tools::getValue('carriers') == '' ? 'all' : Tools::getValue('carriers')));
        $codfeeconf->countries = (is_array(Tools::getValue('countries')) ? (in_array('all', Tools::getValue('countries')) ? 'all' : implode(';', Tools::getValue('countries'))) : (Tools::getValue('countries') == '' ? 'all' : Tools::getValue('countries')));
        $codfeeconf->zones = (is_array(Tools::getValue('zones')) ? (in_array('all', Tools::getValue('zones')) ? 'all' : implode(';', Tools::getValue('zones'))) : (Tools::getValue('zones') == '' ? 'all' : Tools::getValue('zones')));
        $codfeeconf->categories = (is_array(Tools::getValue('categories')) ? (in_array('all', Tools::getValue('categories')) ? 'all' : implode(';', Tools::getValue('categories'))) : (Tools::getValue('categories') == '' ? 'all' : Tools::getValue('categories')));
        $codfeeconf->suppliers = (is_array(Tools::getValue('suppliers')) ? (in_array('all', Tools::getValue('suppliers')) ? 'all' : implode(';', Tools::getValue('suppliers'))) : (Tools::getValue('suppliers') == '' ? 'all' : Tools::getValue('suppliers')));
        $codfeeconf->manufacturers = (is_array(Tools::getValue('manufacturers')) ? (in_array('all', Tools::getValue('manufacturers')) ? 'all' : implode(';', Tools::getValue('manufacturers'))) : (Tools::getValue('manufacturers') == '' ? 'all' : Tools::getValue('manufacturers')));
        $codfeeconf->initial_status = Tools::getValue('initial_status');
        $codfeeconf->show_conf_page = Tools::getValue('show_conf_page');
        $codfeeconf->free_on_freeshipping = Tools::getValue('free_on_freeshipping');
        $codfeeconf->hide_first_order = Tools::getValue('hide_first_order');
        $codfeeconf->active = Tools::getValue('active');
        $codfeeconf->id_shop = $this->context->shop->id;
        foreach (Language::getIsoIds(false) as $lang) {
            $id_lang = $lang['id_lang'];
            $codfeeconf->payment_text[$id_lang] = Tools::getValue('payment_text_'.$id_lang);
        }
        $codfeeconf->priority = Tools::getValue('priority');
        $codfeeconf->save();

        if (Tools::getValue('filename') && Tools::getValue('filename') != '') {
            ImageManager::resize($_FILES['logo']['tmp_name'], _PS_TMP_IMG_DIR_.$this->module_name.'_'.(int)$codfeeconf->id.'.'.$this->imageType);
            ImageManager::resize($_FILES['logo']['tmp_name'], _PS_TMP_IMG_DIR_.(int)$codfeeconf->id.'.'.$this->imageType);
            ImageManager::resize($_FILES['logo']['tmp_name'], _PS_TMP_IMG_DIR_.$this->table.'_mini_'.(int)$codfeeconf->id.'_'.$this->context->shop->id.'.'.$this->imageType, 60);
        }
    }

    public function processUpdate()
    {
        if (Validate::isLoadedObject($this->object)) {
            $this->_formValidations();
            if ($this->object) {
                $codfeeconf = new CodfeeConfiguration((int)$this->object->id_codfee_configuration);
            }
            if (Validate::isLoadedObject($codfeeconf)) {
                $codfeeconf->name = Tools::getValue('name');
                $codfeeconf->type = Tools::getValue('type');
                $codfeeconf->fix = Tools::getValue('fix');
                $codfeeconf->percentage = Tools::getValue('percentage');
                $codfeeconf->min = Tools::getValue('min');
                $codfeeconf->max = Tools::getValue('max');
                $codfeeconf->order_min = Tools::getValue('order_min');
                $codfeeconf->order_max = Tools::getValue('order_max');
                $codfeeconf->amount_free = Tools::getValue('amount_free');
                $codfeeconf->groups = (is_array(Tools::getValue('groups')) ? (in_array('all', Tools::getValue('groups')) ? 'all' : implode(';', Tools::getValue('groups'))) : (Tools::getValue('groups') == '' ? 'all' : Tools::getValue('groups')));
                $codfeeconf->carriers = (is_array(Tools::getValue('carriers')) ? (in_array('all', Tools::getValue('carriers')) ? 'all' : implode(';', Tools::getValue('carriers'))) : (Tools::getValue('carriers') == '' ? 'all' : Tools::getValue('carriers')));
                $codfeeconf->countries = (is_array(Tools::getValue('countries')) ? (in_array('all', Tools::getValue('countries')) ? 'all' : implode(';', Tools::getValue('countries'))) : (Tools::getValue('countries') == '' ? 'all' : Tools::getValue('countries')));
                $codfeeconf->zones = (is_array(Tools::getValue('zones')) ? (in_array('all', Tools::getValue('zones')) ? 'all' : implode(';', Tools::getValue('zones'))) : (Tools::getValue('zones') == '' ? 'all' : Tools::getValue('zones')));
                $codfeeconf->categories = (is_array(Tools::getValue('categories')) ? (in_array('all', Tools::getValue('categories')) ? 'all' : implode(';', Tools::getValue('categories'))) : (Tools::getValue('categories') == '' ? 'all' : Tools::getValue('categories')));
                $codfeeconf->suppliers = (is_array(Tools::getValue('suppliers')) ? (in_array('all', Tools::getValue('suppliers')) ? 'all' : implode(';', Tools::getValue('suppliers'))) : (Tools::getValue('suppliers') == '' ? 'all' : Tools::getValue('suppliers')));
                $codfeeconf->manufacturers = (is_array(Tools::getValue('manufacturers')) ? (in_array('all', Tools::getValue('manufacturers')) ? 'all' : implode(';', Tools::getValue('manufacturers'))) : (Tools::getValue('manufacturers') == '' ? 'all' : Tools::getValue('manufacturers')));
                $codfeeconf->initial_status = Tools::getValue('initial_status');
                $codfeeconf->show_conf_page = Tools::getValue('show_conf_page');
                $codfeeconf->free_on_freeshipping = Tools::getValue('free_on_freeshipping');
                $codfeeconf->hide_first_order = Tools::getValue('hide_first_order');
                $codfeeconf->active = Tools::getValue('active');
                $codfeeconf->id_shop = $this->context->shop->id;
                foreach (Language::getIsoIds(false) as $lang) {
                    $id_lang = $lang['id_lang'];
                    $codfeeconf->payment_text[$id_lang] = Tools::getValue('payment_text_'.$id_lang);
                }
                $codfeeconf->priority = Tools::getValue('priority');
                if (Tools::getValue('filename') && Tools::getValue('filename') != '') {
                    parent::postImage($this->object->id_codfee_configuration);
                }
                $codfeeconf->save();
            }
        } else {
            $this->errors[] = Tools::displayError('An error occurred while loading the object.').'
                <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
        }
    }

    public function postProcess()
    {
        return parent::postProcess();
    }

    public function processSave()
    {
        //$codfeeconf = new CodfeeConfiguration((int)$this->object->id);
        //$this->errors = array_merge($this->errors, $codfeeconf->validateFieldsRequiredDatabase());

        return parent::processSave();
    }

    protected function afterAdd($object)
    {
        $id_codfee_configuration = Tools::getValue('id_codfee_configuration');
        $this->afterUpdate($object, $id_codfee_configuration);
        return true;
    }

    protected function afterUpdate($object, $id_codfee_configuration = false)
    {
        if ($id_codfee_configuration) {
            $codfeeconf = new CodfeeConfiguration((int)$id_codfee_configuration);
        } else {
            $codfeeconf = new CodfeeConfiguration((int)$object->id_codfee_configuration);
        }
        if (Validate::isLoadedObject($codfeeconf)) {
            $codfeeconf->groups = (in_array('all', Tools::getValue('groups'))) ? 'all' : implode(';', Tools::getValue('groups'));
            $codfeeconf->countries = (in_array('all', Tools::getValue('countries'))) ? 'all' : implode(';', Tools::getValue('countries'));
            $codfeeconf->zones = (in_array('all', Tools::getValue('zones'))) ? 'all' : implode(';', Tools::getValue('zones'));
            $codfeeconf->carriers = (in_array('all', Tools::getValue('carriers'))) ? 'all' : implode(';', Tools::getValue('carriers'));
            $codfeeconf->categories = (in_array('all', Tools::getValue('categories'))) ? 'all' : implode(';', Tools::getValue('categories'));
            $codfeeconf->manufacturers = (in_array('all', Tools::getValue('manufacturers'))) ? 'all' : implode(';', Tools::getValue('manufacturers'));
            $codfeeconf->suppliers = (in_array('all', Tools::getValue('suppliers'))) ? 'all' : implode(';', Tools::getValue('suppliers'));
            $codfeeconf->save();
        }
        return true;
    }

    /**
     * Toggle active flag
     */
    public function processChangeActiveVal()
    {
        $codfeeconf = new CodfeeConfiguration($this->id_object);

        if (!Validate::isLoadedObject($codfeeconf)) {
            $this->errors[] = Tools::displayError('An error occurred while updating fee information.');
        }
        $codfeeconf->active = $codfeeconf->active ? 0 : 1;
        if (!$codfeeconf->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating fee information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    /**
     * Toggle show conf page flag
     */
    public function processChangeShowConfPageVal()
    {
        $codfeeconf = new CodfeeConfiguration($this->id_object);

        if (!Validate::isLoadedObject($codfeeconf)) {
            $this->errors[] = Tools::displayError('An error occurred while updating fee information.');
        }
        $codfeeconf->show_conf_page = $codfeeconf->show_conf_page ? 0 : 1;
        if (!$codfeeconf->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating fee information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function processChangeFreeOnFreeShippingVal()
    {
        $codfeeconf = new CodfeeConfiguration($this->id_object);

        if (!Validate::isLoadedObject($codfeeconf)) {
            $this->errors[] = Tools::displayError('An error occurred while updating fee information.');
        }
        $codfeeconf->free_on_freeshipping = $codfeeconf->free_on_freeshipping ? 0 : 1;
        if (!$codfeeconf->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating fee information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function processChangeHideFirstOrderVal()
    {
        $codfeeconf = new CodfeeConfiguration($this->id_object);

        if (!Validate::isLoadedObject($codfeeconf)) {
            $this->errors[] = Tools::displayError('An error occurred while updating fee information.');
        }
        $codfeeconf->hide_first_order = $codfeeconf->hide_first_order ? 0 : 1;
        if (!$codfeeconf->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating fee information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function printActiveIcon($value, $codfeeconf)
    {
        return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminCodfeeConfiguration&id_codfee_configuration='.(int)$codfeeconf['id_codfee_configuration'].'&changeActiveVal&token='.Tools::getAdminTokenLite('AdminCodfeeConfiguration')).'">'.($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').'</a>';
    }

    public function printShowConfPageIcon($value, $codfeeconf)
    {
        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminCodfeeConfiguration&id_codfee_configuration='.(int)$codfeeconf['id_codfee_configuration'].'&changeShowConfPageVal&token='.Tools::getAdminTokenLite('AdminCodfeeConfiguration')).'">'.($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').'</a>';
        } else {
            return $value;
        }
    }

    public function printFreeOnFreeShippingIcon($value, $codfeeconf)
    {
        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminCodfeeConfiguration&id_codfee_configuration='.(int)$codfeeconf['id_codfee_configuration'].'&changeFreeOnFreeShippingVal&token='.Tools::getAdminTokenLite('AdminCodfeeConfiguration')).'">'.($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').'</a>';
        } else {
            return $value;
        }
    }

    public function printHideFirstOrderIcon($value, $codfeeconf)
    {
        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            return '<a class="list-action-enable ' . ($value ? 'action-enabled' : 'action-disabled') . '" href="index.php?' . htmlspecialchars('tab=AdminCodfeeConfiguration&id_codfee_configuration=' . (int)$codfeeconf['id_codfee_configuration'] . '&changeHideFirstOrderVal&token=' . Tools::getAdminTokenLite('AdminCodfeeConfiguration')) . '">' . ($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>') . '</a>';
        } else {
            return $value;
        }
    }

    protected function afterImageUpload()
    {
        $res = true;
        //$generate_hight_dpi_images = (bool)Configuration::get('PS_HIGHT_DPI');
        /* Generate image with differents size */
        if (($id_codfeeconf = (int)Tools::getValue('id_codfee_configuration')) &&
            isset($_FILES) &&
            count($_FILES) &&
            file_exists(_PS_TMP_IMG_DIR_.$id_codfeeconf.'.'.$this->imageType)) {
            /*
            $images_types = ImageType::getImagesTypes('manufacturers');
            foreach ($images_types as $k => $image_type) {
                $res &= ImageManager::resize(
                    _PS_TMP_IMG_DIR_.$id_codfeeconf.'.jpg',
                    _PS_TMP_IMG_DIR_.$id_codfeeconf.'-'.stripslashes($image_type['name']).'.jpg',
                    (int)$image_type['width'],
                    (int)$image_type['height']
                );
                if ($generate_hight_dpi_images) {
                    $res &= ImageManager::resize(
                        _PS_TMP_IMG_DIR_.$id_codfeeconf.'.jpg',
                        _PS_TMP_IMG_DIR_.$id_codfeeconf.'-'.stripslashes($image_type['name']).'2x.jpg',
                        (int)$image_type['width']*2,
                        (int)$image_type['height']*2
                    );
                }
            }
            */
            $current_logo_file = _PS_TMP_IMG_DIR_.$this->table.'_mini_'.$id_codfeeconf.'_'.$this->context->shop->id.'.'.$this->imageType;
            if ($res && file_exists($current_logo_file)) {
                unlink($current_logo_file);
            }
            ImageManager::thumbnail(_PS_TMP_IMG_DIR_.$id_codfeeconf.'.'.$this->imageType, $this->module_name.'_'.$id_codfeeconf.'.'.$this->imageType, 350, $this->imageType, true, true);
        }
        if (!$res) {
            $this->errors[] = Tools::displayError('Unable to resize one or more of your pictures.');
        }
        return $res;
    }

    /**
     * @param string $token
     * @param int $id
     * @param string $name
     * @return mixed
     */
    public function displayDeleteLink($token = null, $id = 0, $name = null)
    {
        $tpl = $this->createTemplate('helpers/list/list_action_delete.tpl');
        $tpl->assign(array(
            'href' => self::$currentIndex.'&'.$this->identifier.'='.$id.'&delete'.$this->table.'&token='.($token != null ? $token : $this->token),
            'confirm' => $this->l('Delete the selected item? ').$name,
            'action' => $this->l('Delete'),
            'id' => $id,
        ));

        return $tpl->fetch();
    }

    protected function getCodfeeTypes()
    {
        $types = array($this->l('Fix'), $this->l('Percentage'), $this->l('Fix + Percentage'));

        $list_types = array();
        foreach ($types as $key => $type) {
            $list_types[$key]['id'] = $key;
            $list_types[$key]['value'] = $key;
            $list_types[$key]['name'] = $type;
        }
        return $list_types;
    }

    public function getCodfeeType($type)
    {
        if ($type == '0') {
            return $this->l('Fix');
        } elseif ($type == '1') {
            return $this->l('Percentage');
        } elseif ($type == '2') {
            return $this->l('Fix + Percentage');
        }
    }

    public function getCustomerGroups($ids_customer_groups)
    {
        if ($ids_customer_groups === 'all') {
            return $this->l('All');
        }
        $groups = array();
        $groups_array = explode(';', $ids_customer_groups);
        foreach ($groups_array as $key => $group) {
            if ($key == $this->top_elements_in_list) {
                $groups[] = $this->l('...and more');
                break;
            }
            $group = new Group($group, $this->context->language->id);
            $groups[] = $group->name;
        }
        return implode('<br />', $groups);
    }

    public function getCarriers($ids_carriers)
    {
        if ($ids_carriers === 'all') {
            return $this->l('All');
        }
        $carriers = array();
        $carriers_array = explode(';', $ids_carriers);
        foreach ($carriers_array as $key => $carrier) {
            if ($key == $this->top_elements_in_list) {
                $carriers[] = $this->l('...and more');
                break;
            }
            $carrier = Carrier::getCarrierByReference($carrier);
            $carriers[] = $carrier->name;
        }
        return implode('<br />', $carriers);
    }

    public function getCountries($ids_countries)
    {
        if ($ids_countries === 'all') {
            return $this->l('All');
        }
        $countries = array();
        $countries_array = explode(';', $ids_countries);
        foreach ($countries_array as $key => $country) {
            if ($key == $this->top_elements_in_list) {
                $countries[] = $this->l('...and more');
                break;
            }
            $country = new Country($country, $this->context->language->id);
            $countries[] = $country->name;
        }
        return implode('<br />', $countries);
    }

    public function getZones($ids_zones)
    {
        if ($ids_zones === 'all') {
            return $this->l('All');
        }
        $zones = array();
        $zones_array = explode(';', $ids_zones);
        foreach ($zones_array as $key => $zone) {
            if ($key == $this->top_elements_in_list) {
                $zones[] = $this->l('...and more');
                break;
            }
            $zone = new Zone($zone);
            $zones[] = $zone->name;
        }
        return implode('<br />', $zones);
    }

    public function getCategories($ids_categories)
    {
        if ($ids_categories === 'all') {
            return $this->l('All');
        }
        $categories = array();
        $categories_array = explode(';', $ids_categories);
        foreach ($categories_array as $key => $category) {
            if ($key == $this->top_elements_in_list) {
                $categories[] = $this->l('...and more');
                break;
            }
            $category = new Category($category, $this->context->language->id);
            $categories[] = $category->name;
        }
        return implode('<br />', $categories);
    }

    private function _createTemplate($tpl_name)
    {
        if ($this->override_folder) {
            if ($this->context->controller instanceof ModuleAdminController) {
                $override_tpl_path = $this->context->controller->getTemplatePath().$tpl_name;
            } elseif ($this->module) {
                $override_tpl_path = _PS_MODULE_DIR_.$this->module_name.'/views/templates/admin/'.$tpl_name;
            } else {
                if (file_exists($this->context->smarty->getTemplateDir(1).DIRECTORY_SEPARATOR.$this->override_folder.$this->base_folder.$tpl_name)) {
                    $override_tpl_path = $this->context->smarty->getTemplateDir(1).DIRECTORY_SEPARATOR.$this->override_folder.$this->base_folder.$tpl_name;
                } elseif (file_exists($this->context->smarty->getTemplateDir(0).DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.$this->override_folder.$this->base_folder.$tpl_name)) {
                    $override_tpl_path = $this->context->smarty->getTemplateDir(0).'controllers'.DIRECTORY_SEPARATOR.$this->override_folder.$this->base_folder.$tpl_name;
                }
            }
        } else if ($this->module) {
            $override_tpl_path = _PS_MODULE_DIR_.$this->module_name.'/views/templates/admin/'.$tpl_name;
        }
        if (isset($override_tpl_path) && file_exists($override_tpl_path)) {
            return $this->context->smarty->createTemplate($override_tpl_path, $this->context->smarty);
        } else {
            return $this->context->smarty->createTemplate($tpl_name, $this->context->smarty);
        }
    }

    private function _formValidations()
    {
        if (trim(Tools::getValue('name')) == '') {
            $this->validateRules();
            $this->errors[] = Tools::displayError($this->l('Field "Name" can not be empty.'));
            $this->display = 'edit';
        }
        if (trim(Tools::getValue('type')) == '') {
            $this->validateRules();
            $this->errors[] = Tools::displayError($this->l('Field "Type" can not be empty.'));
            $this->display = 'edit';
        }
        /*
        if (Tools::getValue('groups') == '') {
            $this->validateRules();
            $this->errors[] = Tools::displayError($this->l('Field "Customer group(s)" can not be empty.'));
            $this->display = 'edit';
        }
        if (Tools::getValue('carriers') == '') {
            $this->validateRules();
            $this->errors[] = Tools::displayError($this->l('Field "Carrier(s) allowed" can not be empty.'));
            $this->display = 'edit';
        }
        if (Tools::getValue('countries') == '') {
            $this->validateRules();
            $this->errors[] = Tools::displayError($this->l('Field "Country(s) allowed" can not be empty.'));
            $this->display = 'edit';
        }
        if (Tools::getValue('zones') == '') {
            $this->validateRules();
            $this->errors[] = Tools::displayError($this->l('Field "Zone(s) allowed" can not be empty.'));
            $this->display = 'edit';
        }
        if (Tools::getValue('categories') == '') {
            $this->validateRules();
            $this->errors[] = Tools::displayError($this->l('Field "Category(s)" can not be empty.'));
            $this->display = 'edit';
        }
        if (Tools::getValue('manufacturers') == '') {
            $this->validateRules();
            $this->errors[] = Tools::displayError($this->l('Field "Manufacturer(s)" can not be empty.'));
            $this->display = 'edit';
        }
        if (Tools::getValue('suppliers') == '') {
            $this->validateRules();
            $this->errors[] = Tools::displayError($this->l('Field "Supplier(s)" can not be empty.'));
            $this->display = 'edit';
        }
        */
        if (trim(Tools::getValue('initial_status')) == '') {
            $this->validateRules();
            $this->errors[] = Tools::displayError($this->l('Field "Initial order status" can not be empty.'));
            $this->display = 'edit';
        }
    }
}
