<?php
/**
 * PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
 *
 * @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
 * @copyright 2010-2016 VEKIA
 * @license   This program is not free software and you can't resell and redistribute it
 *
 * CONTACT WITH DEVELOPER http://mypresta.eu
 * support@mypresta.eu
 */

include_once ('../../config/config.inc.php');
include_once ('../../init.php');
include_once ('hpp.php');


if (Tools::getValue('action') == 'updateSlidesPosition')
{
	$slides = Tools::getValue('homepagebloks'.Tools::getValue('hook'));
	foreach ($slides as $position => $idb)
		$res = Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'hpp_block` SET `position` = '.(int)$position.' WHERE `id` = '.(int)$idb);
}

$thismodule = new hpp();

if (Tools::getValue('search','false') != 'false')
{
	$result = $thismodule->searchproduct(Tools::getValue('search'));
	if (count($result) > 0)
	{
		foreach ($result as $key => $value)
			echo '<p style="display:block; clear:both; padding:0px; padding-top:3px; margin:0px;">'.$value['name'].'<span style="display:inline-block; background:#FFF; cursor:pointer; border:1px solid black; padding:1px 3px;margin-left:5px;" onclick="$(\'.hpp_products\').val($(\'.hpp_products\').val()+\''.$value['id_product'].',\')">'.$thismodule->addproduct.'</span></p>';
	}
	else
		echo $thismodule->noproductsfound;
}
