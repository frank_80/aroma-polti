<?php
/**
 * PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
 *
 * @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
 * @copyright 2010-2016 VEKIA
 * @license   This program is not free software and you can't resell and redistribute it
 *
 * CONTACT WITH DEVELOPER http://mypresta.eu
 * support@mypresta.eu
 */

if (file_exists(_PS_MODULE_DIR_ . 'hpp/models/block.php')) {
    require_once _PS_MODULE_DIR_ . 'hpp/models/block.php';
}

class Hpp extends Module
{
    public function __construct()
    {
        $this->name = 'hpp';
        $this->tab = 'advertising_marketing';
        $this->author = 'MyPresta.eu';
        $this->version = '1.6.7';
        $this->module_key = 'dc40a657e7f92c5557e4a5306a53fff0';
        $this->mypresta_link = 'http://addons.prestashop.com/en/18745-homepage-featured-products-pro.html';
        $this->mkey = "nlc";
        if (@file_exists('../modules/' . $this->name . '/key.php')) {
            @require_once('../modules/' . $this->name . '/key.php');
        } elseif (@file_exists(dirname(__file__) . $this->name . '/key.php')) {
            @require_once(dirname(__file__) . $this->name . '/key.php');
        } elseif (@file_exists('modules/' . $this->name . '/key.php')) {
            @require_once('modules/' . $this->name . '/key.php');
        }
        parent::__construct();
        $this->checkforupdates();
        $this->displayName = $this->l('Homepage Products Pro');
        $this->description = $this->l('Module allows to create & display blocks with products on homepage');
        $this->addproduct = $this->l('Add');
        $this->noproductsfound = $this->l('No products found');
    }

    function checkforupdates($display_msg = 0, $form = 0)
    {
        // --------- //
        // --------- //
        // VERSION 7 //
        // --------- //
        // --------- //
        if ($form == 1) {
            return '
            <div class="panel" id="fieldset_myprestaupdates" style="margin-top:20px;">
            ' . ($this->psversion() == 6 ? '<div class="panel-heading"><i class="icon-wrench"></i> ' . $this->l('MyPresta updates') . '</div>' : '') . '
			<div class="form-wrapper nobootstrap" style="padding:0px!important;">
            <div id="module_block_settings">
                    <fieldset id="fieldset_module_block_settings">
                         ' . ($this->psversion() == 5 ? '<legend style="">' . $this->l('MyPresta updates') . '</legend>' : '') . '
                        <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
                            <label>' . $this->l('Check updates') . '</label>
                            <div class="margin-form">' . (Tools::isSubmit('submit_settings_updates_now') ? ($this->inconsistency(0) ? '' : '') . $this->checkforupdates(1) : '') . '
                                <input style="margin: 0px; top: -3px; position: relative;" type="submit" name="submit_settings_updates_now" value="' . $this->l('Check now') . '" class="button" />
                            </div>
                            <label>' . $this->l('Updates notifications') . '</label>
                            <div class="margin-form">
                                <select name="mypresta_updates">
                                    <option value="-">' . $this->l('-- select --') . '</option>
                                    <option value="1" ' . ((int)(Configuration::get('mypresta_updates') == 1) ? 'selected="selected"' : '') . '>' . $this->l('Enable') . '</option>
                                    <option value="0" ' . ((int)(Configuration::get('mypresta_updates') == 0) ? 'selected="selected"' : '') . '>' . $this->l('Disable') . '</option>
                                </select>
                                <p class="clear">' . $this->l('Turn this option on if you want to check MyPresta.eu for module updates automatically. This option will display notification about new versions of this addon.') . '</p>
                            </div>
                            <label>' . $this->l('Module page') . '</label>
                            <div class="margin-form">
                                <a style="font-size:14px;" href="' . $this->mypresta_link . '" target="_blank">' . $this->displayName . '</a>
                                <p class="clear">' . $this->l('This is direct link to official addon page, where you can read about changes in the module (changelog)') . '</p>
                            </div>
                            <center><input type="submit" name="submit_settings_updates" value="' . $this->l('Save Settings') . '" class="button" /></center>
                        </form>
                    </fieldset>
                </div>
            </div>
        </div>';
        } else {
            if (defined('_PS_ADMIN_DIR_')) {
                if (Tools::isSubmit('submit_settings_updates')) {
                    Configuration::updateValue('mypresta_updates', Tools::getValue('mypresta_updates'));
                }
                if (Configuration::get('mypresta_updates') != 0 || (bool)Configuration::get('mypresta_updates') == false) {
                    if (Configuration::get('update_' . $this->name) < (date("U") - 259200)) {
                        $actual_version = hppUpdate::verify($this->name, (isset($this->mkey) ? $this->mkey : 'nokey'), $this->version);
                    }
                    if (hppUpdate::version($this->version) < hppUpdate::version(Configuration::get('updatev_' . $this->name))) {
                        $this->warning = $this->l('New version available, check http://MyPresta.eu for more informations');
                    }
                }
                if ($display_msg == 1) {
                    if (hppUpdate::version($this->version) < hppUpdate::version(hppUpdate::verify($this->name, (isset($this->mkey) ? $this->mkey : 'nokey'), $this->version))) {
                        return "<span style='color:red; font-weight:bold; font-size:16px;'>" . $this->l('New version available!') . "</span>";
                    } else {
                        return "<span style='color:green; font-weight:bold; font-size:16px;'>" . $this->l('Module is up to date!') . "</span>";
                    }
                }
            }
        }
    }

    public static function psversion($part = 1)
    {
        $version = _PS_VERSION_;
        $exp = explode('.', $version);
        if ($part == 1) {
            return $exp[1];
        }
        if ($part == 2) {
            return $exp[2];
        }
        if ($part == 3) {
            return $exp[3];
        }
    }

    public function install()
    {
        if ($this->psversion() == 5 || $this->psversion() == 6 || $this->psversion() == 7) {
            if (parent::install() == false or !Configuration::updateValue('update_' . $this->name, '0') or !$this->registerHook('home') or !$this->registerHook('displayhomeTab') or !$this->registerHook('leftColumn') or !$this->registerHook('rightColumn') or !$this->registerHook('displayhomeTabContent') or !$this->registerHook('displayHeader') or !$this->installdb()) {
                return false;
            }
        }
        return true;
    }

    private function installdb()
    {
        $prefix = _DB_PREFIX_;
        $statements = array();
        $statements[] = "CREATE TABLE IF NOT EXISTS `${prefix}hpp_block` (" . '`id` int(10) NOT NULL AUTO_INCREMENT,' . '`type` int(3),' . '`block_position` int(3),' . '`active` int(1),' . '`position` int(5),' . '`value` int(5),' . '`nb` int(4),' . '`shop` int(4) DEFAULT 4,' . '`list_products` TEXT,' . '`before` int(1) DEFAULT 0,' . '`after` int(1) DEFAULT 1, ' . ' PRIMARY KEY (`id`)' . ')';
        $statements[] = "CREATE TABLE IF NOT EXISTS `${prefix}hpp_block_lang` (" . '`id` int(10) NOT NULL,' . '`id_lang` int(10) NOT NULL,' . '`name` VARCHAR(230),' . '`custom_before` TEXT,' . '`custom_after` TEXT' . ') COLLATE="utf8_general_ci"';

        foreach ($statements as $statement) {
            if (!Db::getInstance()->Execute($statement)) {
                return false;
            }
        }
        $this->inconsistency();
        return true;
    }


    public function returnProducts($block)
    {
        $blockss = array();
        $blocks = array();
        $blocks[] = new Hppblock($block);
        foreach ($blocks as $key => $value) {
            if ($value->type == 1) {
                $category = new Category($value->value);
                if ($value->random == 1) {
                    $products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb, null, null, null, true, true, $value->nb);
                } else {
                    $products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb);
                }
            }
            if ($value->type == 2) {
                $products = Product::getNewProducts($this->context->language->id, 0, $value->nb);
            }

            if ($value->type == 3) {
                $products = ProductSale::getBestSalesLight($this->context->language->id, 0, $value->nb);
            }

            if ($value->type == 4) {
                $products = Product::getPricesDrop($this->context->language->id, 0, $value->nb, false);
            }

            if ($value->type == 5) {
                $explode = explode(',', $value->list_products);
                foreach ($explode as $tproduct) {
                    if ($tproduct != '') {
                        $x = (array)new Product($tproduct, true, $this->context->language->id);
                        $productss[$tproduct] = $x;
                        $productss[$tproduct]['id_product'] = $tproduct;
                        $image = self::geImagesByID($tproduct, 1);
                        $picture = explode('-', $image[0]);
                        $productss[$tproduct]['id_image'] = $picture[1];
                    }
                }
                $products = Product::getProductsProperties($this->context->language->id, $productss);
            }
        }
        return $products;
    }


    public function hookRightColumn($params)
    {

        $blocks2 = '';
        $blockss = array();
        $blocks = Hppblock::getAllBlocksByPosition(3);
        foreach ($blocks as $key => $value) {
            if ($value->type == 1) {
                $category = new Category($value->value);
                if ($value->random == 1) {
                    $blocks[$key]->products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb, null, null, null, true, true, $value->nb);
                } else {
                    $blocks[$key]->products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb);
                }
            }
            if ($value->type == 2) {
                $blocks[$key]->products = Product::getNewProducts($this->context->language->id, 0, $value->nb);
            }

            if ($value->type == 3) {
                $blocks[$key]->products = ProductSale::getBestSalesLight($this->context->language->id, 0, $value->nb);
            }

            if ($value->type == 4) {
                $blocks[$key]->products = Product::getPricesDrop($this->context->language->id, 0, $value->nb, false);
            }

            if ($value->type == 5) {
                $explode = explode(',', $value->list_products);
                foreach ($explode as $tproduct) {
                    if ($tproduct != '') {
                        $x = (array)new Product($tproduct, true, $this->context->language->id);
                        $blockss[$key]->products[$tproduct] = $x;
                        $blockss[$key]->products[$tproduct]['id_product'] = $tproduct;
                        $image = self::geImagesByID($tproduct, 1);
                        $picture = explode('-', $image[0]);
                        $blockss[$key]->products[$tproduct]['id_image'] = $picture[1];
                    }
                }

                $blocks[$key]->products = Product::getProductsProperties($this->context->language->id, $blockss[$key]->products);

            }
        }

        if ($this->psversion() == 5) {
            $this->smarty->assign(array(
                'blocks' => $blocks,
                'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
            ));
        } elseif ($this->psversion() == 6 || $this->psversion() == 7) {
            $this->smarty->assign(array(
                'blocks' => $blocks,
                'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
            ));
        }

        if ($this->psversion() == 6 || $this->psversion() == 7) {
            return $this->display(__file__, 'column16.tpl');
        }
        if ($this->psversion() == 5) {
            return $this->display(__file__, 'column15.tpl');
        }

    }

    public function hookLeftColumn($params)
    {

        $blocks2 = '';
        $blockss = array();
        $blocks = Hppblock::getAllBlocksByPosition(2);
        foreach ($blocks as $key => $value) {
            if ($value->type == 1) {
                $category = new Category($value->value);
                if ($value->random == 1) {
                    $blocks[$key]->products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb, null, null, null, true, true, $value->nb);
                } else {
                    $blocks[$key]->products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb);
                }
            }
            if ($value->type == 2) {
                $blocks[$key]->products = Product::getNewProducts($this->context->language->id, 0, $value->nb);
            }

            if ($value->type == 3) {
                $blocks[$key]->products = ProductSale::getBestSalesLight($this->context->language->id, 0, $value->nb);
            }

            if ($value->type == 4) {
                $blocks[$key]->products = Product::getPricesDrop($this->context->language->id, 0, $value->nb, false);
            }

            if ($value->type == 5) {
                $explode = explode(',', $value->list_products);
                foreach ($explode as $tproduct) {
                    if ($tproduct != '') {
                        $x = (array)new Product($tproduct, true, $this->context->language->id);
                        $blockss[$key]->products[$tproduct] = $x;
                        $blockss[$key]->products[$tproduct]['id_product'] = $tproduct;
                        $image = self::geImagesByID($tproduct, 1);
                        $picture = explode('-', $image[0]);
                        $blockss[$key]->products[$tproduct]['id_image'] = $picture[1];
                    }
                }
                $blocks[$key]->products = Product::getProductsProperties($this->context->language->id, $blockss[$key]->products);
            }
        }

        if ($this->psversion() == 5) {
            $this->smarty->assign(array(
                'blocks' => $blocks,
                'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
            ));
        } elseif ($this->psversion() == 6 || $this->psversion() == 7) {
            $this->smarty->assign(array(
                'blocks' => $blocks,
                'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
            ));
        }

        if ($this->psversion() == 6 || $this->psversion() == 7) {
            return $this->display(__file__, 'column16.tpl');
        }
        if ($this->psversion() == 5) {
            return $this->display(__file__, 'column15.tpl');
        }

    }

    public function hookHome($params)
    {
        $blocks2 = '';
        $blockss = array();
        if (Configuration::get('hpp_internal_tabs') == 1) {
            $blocks = Hppblock::getAllBlocksByPosition(0);
            foreach ($blocks as $key => $value) {
                if ($value->type == 1) {
                    $category = new Category($value->value);
                    if ($value->random == 1) {
                        $blocks[$key]->products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb, null, null, null, true, true, $value->nb);
                    } else {
                        $blocks[$key]->products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb);
                    }
                }
                if ($value->type == 2) {
                    $blocks[$key]->products = Product::getNewProducts($this->context->language->id, 0, $value->nb);
                }

                if ($value->type == 3) {
                    $blocks[$key]->products = ProductSale::getBestSalesLight($this->context->language->id, 0, $value->nb);
                }

                if ($value->type == 4) {
                    $blocks[$key]->products = Product::getPricesDrop($this->context->language->id, 0, $value->nb, false);
                }

                if ($value->type == 5) {
                    $explode = explode(',', $value->list_products);
                    foreach ($explode as $tproduct) {
                        if ($tproduct != '') {
                            $x = (array)new Product($tproduct, true, $this->context->language->id);
                            $blockss[$key]->products[$tproduct] = $x;
                            $blockss[$key]->products[$tproduct]['id_product'] = $tproduct;
                            $image = self::geImagesByID($tproduct, 1);
                            $picture = explode('-', $image[0]);
                            $blockss[$key]->products[$tproduct]['id_image'] = $picture[1];
                        }
                    }
                    $blocks[$key]->products = Product::getProductsProperties($this->context->language->id, $blockss[$key]->products);

                }
            }

            if ($this->psversion() == 5) {
                $this->smarty->assign(array(
                    'blocks' => $blocks,
                    'blocks2' => $blocks2,
                    'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
                ));
            } elseif ($this->psversion() == 6 || $this->psversion() == 7) {
                $this->smarty->assign(array(
                    'blocks' => $blocks,
                    'blocks2' => $blocks2,
                    'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
                ));
            }
            if ($this->psversion() == 6 || $this->psversion() == 7) {
                return $this->display(__file__, 'internal-tabs.tpl');
            }

            if ($this->psversion() == 5) {
                return $this->display(__file__, 'internal-tabs.tpl');
            }
        } else {
            $blocks = Hppblock::getAllBlocksByPosition(1);
            foreach ($blocks as $key => $value) {
                if ($value->type == 1) {
                    $category = new Category($value->value);
                    if ($value->random == 1) {
                        $blocks[$key]->products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb, null, null, null, true, true, $value->nb);
                    } else {
                        $blocks[$key]->products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb);
                    }
                }
                if ($value->type == 2) {
                    $blocks[$key]->products = Product::getNewProducts($this->context->language->id, 0, $value->nb);
                }

                if ($value->type == 3) {
                    $blocks[$key]->products = ProductSale::getBestSalesLight($this->context->language->id, 0, $value->nb);
                }

                if ($value->type == 4) {
                    $blocks[$key]->products = Product::getPricesDrop($this->context->language->id, 0, $value->nb, false);
                }

                if ($value->type == 5) {
                    $explode = explode(',', $value->list_products);
                    foreach ($explode as $tproduct) {
                        if ($tproduct != '') {
                            $x = (array)new Product($tproduct, true, $this->context->language->id);
                            $blockss[$key]->products[$tproduct] = $x;
                            $blockss[$key]->products[$tproduct]['id_product'] = $tproduct;
                            $image = self::geImagesByID($tproduct, 1);
                            $picture = explode('-', $image[0]);
                            $blockss[$key]->products[$tproduct]['id_image'] = $picture[1];
                        }
                    }

                    $blocks[$key]->products = Product::getProductsProperties($this->context->language->id, $blockss[$key]->products);

                }
            }

            if ($this->psversion() == 5) {
                $this->smarty->assign(array(
                    'blocks' => $blocks,
                    'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
                ));
            } elseif ($this->psversion() == 6 || $this->psversion() == 7) {
                $this->smarty->assign(array(
                    'blocks' => $blocks,
                    'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
                ));
            }

            if ($this->psversion() == 6 || $this->psversion() == 7) {
                return $this->display(__file__, 'products16.tpl');
            }
            if ($this->psversion() == 5) {
                return $this->display(__file__, 'products.tpl');
            }
        }
    }

    public function hookdisplayHomeTab($params)
    {
        $blocks = Hppblock::getAllBlocksByPosition(0);
        $this->smarty->assign(array('blocks' => $blocks));
        return $this->display(__file__, 'tab16.tpl');
    }

    public function hookdisplayHomeTabContent($params)
    {
        $blockss = array();
        $blocks = Hppblock::getAllBlocksByPosition(0);
        foreach ($blocks as $key => $value) {
            if ($value->type == 1) {
                $category = new Category($value->value);
                if ($value->random == 1) {
                    $blocks[$key]->products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb, null, null, null, true, true, $value->nb);
                } else {
                    $blocks[$key]->products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb);
                }
            }
            if ($value->type == 2) {
                $blocks[$key]->products = Product::getNewProducts($this->context->language->id, 0, $value->nb);
            }

            if ($value->type == 3) {
                $blocks[$key]->products = ProductSale::getBestSalesLight($this->context->language->id, 0, $value->nb);
            }

            if ($value->type == 4) {
                $blocks[$key]->products = Product::getPricesDrop($this->context->language->id, 0, $value->nb, false);
            }

            if ($value->type == 5) {
                $explode = explode(',', $value->list_products);
                foreach ($explode as $tproduct) {
                    if ($tproduct != '') {
                        $x = (array)new Product($tproduct, true, $this->context->language->id);
                        $blockss[$key]->products[$tproduct] = $x;
                        $blockss[$key]->products[$tproduct]['id_product'] = $tproduct;
                        $image = self::geImagesByID($tproduct, 1);
                        $picture = explode('-', $image[0]);
                        $blockss[$key]->products[$tproduct]['id_image'] = $picture[1];
                    }
                }
                $blocks[$key]->products = Product::getProductsProperties($this->context->language->id, $blockss[$key]->products);

            }
        }

        $this->smarty->assign(array(
            'blocks' => $blocks,
            'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
        ));
        if ($this->psversion() == 6 || $this->psversion() == 7) {
            return $this->display(__file__, 'tabContainer16.tpl');
        }
    }

    public function msg_saved()
    {
        return '<div class="conf confirm">' . $this->l('Saved') . '</div>';
    }

    public function getContent()
    {
        $output = '';

        if (Tools::isSubmit('selecttab')) {
            Configuration::updateValue('hpp_lasttab', Tools::getValue('selecttab'));
        }

        if (Tools::isSubmit('add_new_block')) {
            $block = new hppBlock();
            $block->name = Tools::getValue('name');
            $block->type = Tools::getValue('hpp_type');
            $block->shop = $this->context->shop->id;
            $block->block_position = Tools::getValue('hpp_block_position');
            $block->active = Tools::getValue('hpp_active');
            $block->value = Tools::getValue('hpp_value');
            $block->nb = Tools::getValue('hpp_nb');
            $block->list_products = Tools::getValue('hpp_products');
            $block->before = Tools::getValue('hpp_before');
            $block->carousell = Tools::getValue('hpp_carousell');
            $block->carousell_auto = Tools::getValue('hpp_carousell_auto');
            $block->carousell_nb = Tools::getValue('hpp_carousell_nb');
            $block->carousell_nb_mobile = Tools::getValue('hpp_carousell_nb_mobile');
            $block->custom_before = Tools::getValue('custombefore');
            $block->random = Tools::getValue('hpp_random');
            $block->head_url = Tools::getValue('hpp_head_url');
            $block->url = Tools::getValue('url');
            $block->c_al = Tools::getValue('hpp_c_al');
            $block->c_bl = Tools::getValue('hpp_c_bl');
            $block->custom_bl = Tools::getValue('cbl');
            $block->custom_al = Tools::getValue('cal');
            $block->add();
            $output .= $this->msg_saved();
        }
        if (Tools::isSubmit('edit_block')) {
            $block = new hppBlock(Tools::getValue('editblock'));
            $block->name = Tools::getValue('name');
            $block->type = Tools::getValue('hpp_type');
            $block->shop = $this->context->shop->id;
            $block->block_position = Tools::getValue('hpp_block_position');
            $block->active = Tools::getValue('hpp_active');
            $block->value = Tools::getValue('hpp_value');
            $block->nb = Tools::getValue('hpp_nb');
            $block->list_products = Tools::getValue('hpp_products');
            $block->before = Tools::getValue('hpp_before');
            $block->carousell = Tools::getValue('hpp_carousell');
            $block->carousell_auto = Tools::getValue('hpp_carousell_auto');
            $block->carousell_nb = Tools::getValue('hpp_carousell_nb');
            $block->carousell_nb_mobile = Tools::getValue('hpp_carousell_nb_mobile');
            $block->custom_before = Tools::getValue('custombefore');
            $block->random = Tools::getValue('hpp_random');
            $block->head_url = Tools::getValue('hpp_head_url');
            $block->url = Tools::getValue('url');
            $block->c_al = Tools::getValue('hpp_c_al');
            $block->c_bl = Tools::getValue('hpp_c_bl');
            $block->custom_bl = Tools::getValue('cbl');
            $block->custom_al = Tools::getValue('cal');
            $block->update();
            $output .= $this->msg_saved();
        }

        if (Tools::isSubmit('removeblock')) {
            $block = new hppBlock((int)Tools::getValue('removeblock'));
            $block->delete();
            $output .= $this->msg_saved();
        }
        if (Tools::isSubmit('turn_on')) {
            $block = new hppBlock((int)Tools::getValue('turn_on'));
            $block->active = 1;
            $block->update();
            $output .= $this->msg_saved();
        }
        if (Tools::isSubmit('turn_off')) {
            $block = new hppBlock((int)Tools::getValue('turn_off'));
            $block->active = 0;
            $block->update();
            $output .= $this->msg_saved();
        }

        if (Tools::isSubmit('hpp_internal_tabs')) {
            Configuration::updateValue('hpp_internal_tabs', Tools::getValue('hpp_internal_tabs'));
            Configuration::updateValue('hpp_rich_text', Tools::getValue('hpp_rich_text'));
            Configuration::updateValue('hpp_bxslider', Tools::getValue('hpp_bxslider'));
            $output .= $this->msg_saved();
        }

        $output .= '';
        return $output . $this->displayForm();
    }

    public function hookHeader($params)
    {
        if (Configuration::get('hpp_internal_tabs') == 1) {
            $this->context->controller->addCss(($this->_path) . 'css/internal-tabs.css', 'all');
        }

        if ($this->psversion() != 6 || $this->psversion() == 7) {
            $this->context->controller->addCss(($this->_path) . 'css/hpp.css', 'all');
        } else {
            $this->context->controller->addCSS(_THEME_CSS_DIR_ . 'product_list.css');
        }

        if (Configuration::get('hpp_bxslider') == 1) {
            $this->context->controller->addJqueryPlugin(array('bxslider'));
        }

        $this->context->controller->addCss(($this->_path) . 'css/global.css', 'all');
        $this->context->controller->addCss(($this->_path) . 'css/homefeaturedslider.css', 'all');
    }

    public function searchproduct($search)
    {
        return Db::getInstance()->ExecuteS('SELECT `id_product`,`name` FROM `' . _DB_PREFIX_ . 'product_lang` WHERE `name` like "%' . (string)$search . '%" AND id_lang="' . Configuration::get('PS_LANG_DEFAULT') . '" AND id_shop="' . (int)$this->context->shop->id . '" LIMIT 10');
    }

    public function runStatement($statement)
    {
        if (@!Db::getInstance()->Execute($statement)) {
            return false;
        }
        return true;
    }

    public static function geImagesByID($id_product, $limit = 0)
    {
        $id_image = Db::getInstance()->ExecuteS('SELECT `id_image` FROM `' . _DB_PREFIX_ . 'image` WHERE cover=1 AND `id_product` = ' . (int)$id_product . ' ORDER BY position ASC LIMIT 0, ' . (int)$limit);
        $toReturn = array();
        if (!$id_image) {
            return;
        } else {
            foreach ($id_image as $image) {
                $toReturn[] = $id_product . '-' . $image['id_image'];
            }
        }
        return $toReturn;
    }

    public function displayForm()
    {
        $output = '';
        $form = '';
        $form .= '<div id="topmenu-horizontal-module">
            <div class="addnew" onclick="selectform3.submit();"><span class="img"></span><span class="txt">' . $this->l('Add new') . '</span></div>
            <div class="btnpro back" onclick="selectform1.submit();"><span class="img"></span><span class="txt">' . $this->l('list') . '</span></div>
            </div>';
        $this->context->controller->addJqueryUI('ui.sortable');


        if (Configuration::get('hpp_lasttab') == 1) {
            $c_bl = '';
            $c_al = '';
            $url = '';
            $title = '';
            $custom_before = '';
            if (Tools::isSubmit('editblock')) {
                $block = new hppBlock((int)Tools::getValue('editblock'));
                $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
                foreach ($languages = Language::getLanguages(false) as $language) {
                    $title .= '<div id="hppname_' . $language['id_lang'] . '" style="display: ' . ($language['id_lang'] == $id_lang_default ? 'block' : 'none') . ';"><input value="' . $block->name[$language['id_lang']] . '" type="text" id="name_' . $language['id_lang'] . '" name="name[' . $language['id_lang'] . ']"></div>';

                    $url .= '
                         <div id="hppurl_' . $language['id_lang'] . '" style=" display: ' . ($language['id_lang'] == $id_lang_default ? 'block' : 'none') . '; ">
                          <input value="' . $block->url[$language['id_lang']] . '" type="text" id="url_' . $language['id_lang'] . '" name="url[' . $language['id_lang'] . ']">
                         </div>';
                }
                $title .= '<div class="flags_block">' . $this->displayFlags($languages, $id_lang_default, 'hppname', 'hppname', true) . "</div>";
                $url .= '<div class="flags_block">' . $this->displayFlags($languages, $id_lang_default, 'hppurl', 'hppurl', true) . '</div>';

                foreach ($languages = Language::getLanguages(false) as $language) {
                    $custom_before .= '<div id="hppcustombefore_' . $language['id_lang'] . '" style="width:100%; margin-bottom:10px; clear:both; display: ' . ($language['id_lang'] == $id_lang_default ? 'block' : 'none') . '; float: left;"><textarea type="text" id="custombefore_' . $language['id_lang'] . '" class="rte rtepro" name="custombefore[' . $language['id_lang'] . ']">' . $block->custom_before[$language['id_lang']] . '</textarea></div>';
                    $c_bl .= '<div id="hppcbl_' . $language['id_lang'] . '" style="display: ' . ($language['id_lang'] == $id_lang_default ? 'block' : 'none') . '; float: left;"><textarea id="cbl_' . $language['id_lang'] . '" class="rte rtepro" name="cbl[' . $language['id_lang'] . ']">' . $block->custom_bl[$language['id_lang']] . '</textarea></div>';
                    $c_al .= '<div id="hppcal_' . $language['id_lang'] . '" style="display: ' . ($language['id_lang'] == $id_lang_default ? 'block' : 'none') . '; float: left;"><textarea id="cal_' . $language['id_lang'] . '" class="rte rtepro" name="cal[' . $language['id_lang'] . ']">' . $block->custom_al[$language['id_lang']] . '</textarea></div>';
                }
                $custom_before .= '<div class="flags_block">' . $this->displayFlags($languages, $id_lang_default, 'hppcustombefore', 'hppcustombefore', true) . '</div>';
                $c_bl .= '<div class="flags_block">' . $this->displayFlags($languages, $id_lang_default, 'hppcbl', 'hppcbl', true) . '</div>';
                $c_al .= '<div class="flags_block">' . $this->displayFlags($languages, $id_lang_default, 'hppcal', 'hppcal', true) . '</div>';

                $languages = Language::getLanguages(true);
                $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
                global $cookie;
                $iso = Language::getIsoById((int)($cookie->id_lang));
                $isoTinyMCE = (file_exists(_PS_ROOT_DIR_ . '/js/tiny_mce/langs/' . $iso . '.js') ? $iso : 'en');
                $ad = dirname($_SERVER["PHP_SELF"]);

                $form .= (Configuration::get('hpp_rich_text') == 1 ? '<script type="text/javascript" src="' . __PS_BASE_URI__ . 'js/tiny_mce/tiny_mce.js"></script>
                        <script>
                            var iso = \'' . $isoTinyMCE . '\' ;
                    		      var pathCSS = \'' . _THEME_CSS_DIR_ . '\' ;
                    		      var ad = \'' . $ad . '\';
                        </script>
                        <script type="text/javascript" src="../modules/hpp/js/tinymce16-force-urls.inc.js"></script>' : '');


                $form .= '
                <fieldset>
                    <legend><img src="' . $this->_path . 'logo.gif" alt="" title="" />' . $this->l('Block specification') . '</legend>
                    <form name="globalsettings" id="globalsettings" method="post">
                        <h3>' . $this->l('Appearance settings') . '</h3>
                        <div style="padding:10px; margin:10px 0px; border:1px solid #cecece; background:#fefefe; display:block; clear:both;">
                            <label>' . $this->l('Active') . '</label>
                            <div class="margin-form">
                                <select type="text" name="hpp_active" style="max-width:200px;">
                                    <option value="0" ' . ($block->active == 0 ? 'selected="yes"' : '') . '>' . $this->l('No') . '</option>
                                    <option value="1" ' . ($block->active == 1 ? 'selected="yes"' : '') . '>' . $this->l('Yes') . '</option>
                                </select>
                            </div>
                            <label>' . $this->l('Method of appearing') . '</label>
                            <div class="margin-form">
                                <select type="text" name="hpp_block_position" style="max-width:200px;">
                                    <option value="1" ' . ($block->block_position == 1 ? 'selected="yes"' : '') . '>' . $this->l('Block') . '</option>
                                    <option value="0" ' . ($block->block_position == 0 ? 'selected="yes"' : '') . '>' . $this->l('Tab') . '</option>
                                    <option value="2" ' . ($block->block_position == 2 ? 'selected="yes"' : '') . '>' . $this->l('Left Column Block') . '</option>
                                    <option value="3" ' . ($block->block_position == 3 ? 'selected="yes"' : '') . '>' . $this->l('Right Column Block') . '</option>
                                </select>
                            </div>
           				         </div>

                        <h3>' . $this->l('Title') . '</h3>
                        <div style="padding:10px; margin:10px 0px; border:1px solid #cecece; background:#fefefe; display:block; clear:both;">
                            <label>' . $this->l('Name of block / tab') . '</label>
                            <div class="margin-form">
                                ' . $title . '
                            </div>
                            <label>' . $this->l('Add link to name of block') . '</label>
                            <div class="margin-form">
                                <select name="hpp_head_url">
                                    <option value="0" ' . ($block->head_url != 1 ? 'selected="yes"' : '' . '') . '>' . $this->l('No') . '</option>
                                    <option value="1" ' . ($block->head_url == 1 ? 'selected="yes"' : '' . '') . '>' . $this->l('Yes') . '</option>
                                </select>
                            </div>
                            <label>' . $this->l('Link (url)') . '</label>
                            <div class="margin-form" style="">
                            ' . $url . '
                            </div>
                        </div>

                        <h3>' . $this->l('Products') . '</h3>
                        <div style="padding:10px; margin:10px 0px; border:1px solid #cecece; background:#fefefe; display:block; clear:both;">
                            <label>' . $this->l('What to display?') . '</label>
                            <div class="margin-form">
                                <select type="text" name="hpp_type" id="hpp_type" style="max-width:200px;" onchange="if($(\'#hpp_type\').val()==5){$(\'#hpp_type_nb\').hide(); $(\'#hpp_type_products\').show();} else {$(\'#hpp_type_products\').hide(); $(\'#hpp_type_nb\').show();} if($(\'#hpp_type\').val()==1){$(\'#hpp_type_category\').show();}else{$(\'#hpp_type_category\').hide();}">
                                    <option value="1" ' . ($block->type == 1 ? 'selected="yes"' : '') . '>' . $this->l('Products from category') . '</option>
                                    <option value="2" ' . ($block->type == 2 ? 'selected="yes"' : '') . '>' . $this->l('New products') . '</option>
                                    <option value="3" ' . ($block->type == 3 ? 'selected="yes"' : '') . '>' . $this->l('Best sellers') . '</option>
                                    <option value="4" ' . ($block->type == 4 ? 'selected="yes"' : '') . '>' . $this->l('Specials') . '</option>
                                    <option value="5" ' . ($block->type == 5 ? 'selected="yes"' : '') . '>' . $this->l('Selected products') . '</option>
                                </select>
                            </div>
                            <div id="hpp_type_products" style="' . ($block->type != 5 ? 'display:none;' : '') . '">
                                <label>' . $this->l('Products') . '</label>
                                <div class="margin-form">
                                    <textarea name="hpp_products" class="hpp_products">' . $block->list_products . '</textarea>
                                    <p class="">' . $this->l('enter above ID numbers of products you want to display, or search for products below.') . ' <a href="http://mypresta.eu/en/art/basic-tutorials/how-to-get-product-id-in-prestashop.html" target="_blank">' . $this->l('How to get product ID number') . '</a></p>
                                </div>
                                <label>' . $this->l('Search for product') . '</label>
                                <div class="margin-form">
                                    <input type="text" name="hpp_search" class="hpp_search" style="max-width:200px;"> ' . $this->l('') . '</a>
                                    <div id="hpp_search_result" style="margin-top:10px;">
                                    </div>
                                </div>
                            </div>

                            <div id="hpp_type_category" style="' . ($block->type != 1 ? 'display:none;' : '') . '">
                               <label>' . $this->l('Category to display') . '</label>
                               <div class="margin-form">
                                   <input type="text" name="hpp_value" value="' . $block->value . '" style="max-width:200px;"> ' . $this->l('Type here ID of category you want to display. Read') . ' <a href="http://mypresta.eu/en/art/basic-tutorials/prestashop-how-to-get-category-id.html" target="_blank">' . $this->l('how to get Category ID') . '</a>
                               </div>
                               <label>' . $this->l('Random order') . '</label>
                               <div class="margin-form">
                                   <select name="hpp_random">
                                       <option value="0" ' . ($block->random != 1 ? 'selected="yes"' : '' . '') . '>' . $this->l('No') . '</option>
                                       <option value="1" ' . ($block->random == 1 ? 'selected="yes"' : '' . '') . '>' . $this->l('Yes') . '</option>
                                   </select>
                               </div>
                            </div>
                            <div id="hpp_type_nb" style="' . ($block->type == 5 ? 'display:none;' : '') . '">
                               <label>' . $this->l('Number of products you want to display') . '</label>
                               <div class="margin-form">
                                    <input type="text" name="hpp_nb" value="' . $block->nb . '" style="max-width:200px;">
                               </div>
                            </div>
                        </div>

                   <div style=' . ($this->psversion() != 6 && $this->psversion() != 7 ? 'display:none;' : '') . '>
                   <h3>' . $this->l('Carousell feature') . '</h3>
                   <div style="padding:10px; margin:10px 0px; border:1px solid #cecece; background:#fefefe; display:block; clear:both;">
                       <div>
                           <label>' . $this->l('Carousell') . '</label>
                           <div class="margin-form">
                               <select type="text" name="hpp_carousell" style="max-width:200px;">
                               <option value="0" ' . ($block->carousell == 0 ? 'selected="yes"' : '') . '>' . $this->l('No') . '</option>
                               <option value="1" ' . ($block->carousell == 1 ? 'selected="yes"' : '') . '>' . $this->l('Yes') . '</option>
                               </select> ' . $this->l('This feature requires default PrestaShop carousell script: BxSlider. If your theme doesnt support it, please go to module global settings and enable library. Carousell works only in Blocks (not tabs)') . '
                           </div>
                       </div>
                       <div>
                           <label>' . $this->l('Autoplay') . '</label>
                           <div class="margin-form">
                               <select type="text" name="hpp_carousell_auto" style="max-width:200px;">
                                   <option value="0" ' . ($block->carousell_auto == 0 ? 'selected="yes"' : '') . '>' . $this->l('No') . '</option>
                                   <option value="1" ' . ($block->carousell_auto == 1 ? 'selected="yes"' : '') . '>' . $this->l('Yes') . '</option>
                               </select> ' . $this->l('Autostart the carousell, this option will play carousell automatically') . '
                           </div>
                       </div>
                       <label>' . $this->l('Number of products') . '</label>
                       <div class="margin-form">
                            <input type="text" name="hpp_carousell_nb" style="max-width:200px;" value="' . $block->carousell_nb . '"><br/>
                            ' . $this->l('Number of products that will appear as a base of the carousell') . '
                       </div>
                       <label>' . $this->l('Number of products for mobile devices') . '</label>
                       <div class="margin-form">
                            <input type="text" name="hpp_carousell_nb_mobile" style="max-width:200px;" value="' . $block->carousell_nb_mobile . '"><br/>
                            ' . $this->l('Number of products that will appear as a base of the carousell on mobile devices') . '
                       </div>
                   </div>
                   </div>

                <h3>' . $this->l('Custom contents') . '</h3>
                <div style="padding:10px; margin:10px 0px; border:1px solid #cecece; background:#fefefe; display:block; clear:both;">
                    <label>' . $this->l('First custom code element') . '</label>
                    <div class="margin-form">
                        <select type="text" id="hpp_before"  name="hpp_before" style="max-width:200px;" onchange="if($(\'#hpp_before\').val()==1){$(\'#customcodebefore\').show();}else{$(\'#customcodebefore\').hide();}">
                            <option value="0" ' . ($block->before == 0 ? 'selected="yes"' : '') . '>' . $this->l('No') . '</option>
                            <option value="1" ' . ($block->before == 1 ? 'selected="yes"' : '') . '>' . $this->l('Yes') . '</option>
                        </select>
                        <p class="">' . $this->l('Turn this option on if you want to add custom code block as first element of list.') . '<br/>' . $this->l('This option works only with one row of products') . '</p>
           				     </div>
                    <div id="customcodebefore" style="' . ($block->before == 1 ? 'display:block;' : 'display:none;') . '">
                        <label>' . $this->l('Custom code') . '</label>
                        <div class="margin-form">' . $custom_before . '
                            <p class="">' . $this->l('Enter custom html code above') . '</p>
                            <p class="">' . $this->l('Example of code for bootstrap templates.') . '</p>
                            <textarea style="resize:none; box-shadow: 0 1px 2px rgba(0,0,0,0.0) inset; background:none; border:0px; width:300px; height:50px;"><li class="ajax_block_product col-xs-12 col-sm-4 col-md-3">' . $this->l('Example of custom contents') . '</li></textarea>
                        </div>
                    </div>

                   <label>' . $this->l('Custom contents before list of products') . '</label>
                   <div class="margin-form">
                       <select type="text" id="hpp_c_bl" name="hpp_c_bl" style="max-width:200px;" onchange="if($(\'#hpp_c_bl\').val()==1){$(\'#custom_bl\').show();}else{$(\'#custom_bl\').hide();}">
                           <option value="0" ' . ($block->c_bl == 0 ? 'selected="yes"' : '') . '>' . $this->l('No') . '</option>
                           <option value="1" ' . ($block->c_bl == 1 ? 'selected="yes"' : '') . '>' . $this->l('Yes') . '</option>
                       </select>
                       <p class="">' . $this->l('Turn this option on if you want to add custom contents before the list of products') . '</p>
                   </div>
                   <div id="custom_bl" style="' . ($block->c_bl == 1 ? 'display:block;' : 'display:none;') . '">
                       <label>' . $this->l('Custom code') . '</label>
                       <div class="margin-form">' . $c_bl . '</div>
                   </div>

                   <label>' . $this->l('Custom contents after list of products') . '</label>
                   <div class="margin-form">
                       <select type="text" id="hpp_c_al" name="hpp_c_al" style="max-width:200px;" onchange="if($(\'#hpp_c_al\').val()==1){$(\'#custom_al\').show();}else{$(\'#custom_al\').hide();}">
                           <option value="0" ' . ($block->c_al == 0 ? 'selected="yes"' : '') . '>' . $this->l('No') . '</option>
                           <option value="1" ' . ($block->c_al == 1 ? 'selected="yes"' : '') . '>' . $this->l('Yes') . '</option>
                       </select>
                       <p class="">' . $this->l('Turn this option on if you want to add custom contents after the list of products') . '</p>
                   </div>
                   <div id="custom_al" ' . ($block->c_al == 1 ? 'display:block;' : 'display:none;') . '>
                    <label>' . $this->l('Custom code') . '</label>
                    <div class="margin-form">' . $c_al . '</div>
                   </div>
                </div>
                <h3>' . $this->l('Shortcode for CMS pages') . '</h3>
                <div style="padding:10px; margin:10px 0px; border:1px solid #cecece; background:#fefefe; display:block; clear:both;">
                    <a href="https://mypresta.eu/modules/front-office-features/products-on-cms-pages.html" target="_blank">' . $this->l('1.  Install free cms products module') . '</a><br/>
                    ' . $this->l('2.  Use shortcode inside CMS page contents') . ': {hpp:' . $block->id . '}<br/>
                </div>
               <input type="hidden" name="selecttab" value="1">
               <input type="hidden" name="editblock" value="' . Tools::getValue('editblock') . '">
               <input type="submit" class="extra button" name="edit_block" value="' . $this->l('Save changes') . ' "/>
               </form>
               </fieldset>';
            }

            $blocks = HppBlock::getAllBlocks();
            $li = '';
            foreach ($blocks as $k => $v) {
                if ($v->active == 1) {
                    $active = '
                    <form action="" method="post" name="turnoffslide' . $v->id . '"/>
                        <input type="hidden" name="turn_off" value="' . $v->id . '"/>
                    </form>
                    <span onclick="turnoffslide' . $v->id . '.submit()" title="' . $this->l('Turn off') . '"><img src="' . _MODULE_DIR_ . $this->name . '/img/pro_on.png" alt="' . $this->l('Turn off') . '" /></span>';
                } else {
                    $active = '<form action="" method="post" name="turnonslide' . $v->id . '"/>
                                <input type="hidden" name="turn_on" value="' . $v->id . '"/>
                               </form>
                                <span onclick="turnonslide' . $v->id . '.submit()"><img src="' . _MODULE_DIR_ . $this->name . '/img/pro_off.png" alt="' . $this->l('Turn on') . '" /></span>';
                }

                $li .= '<li id="homepagebloks_' . $k . '">' . "{$v->name}
                  <span class=\"remove\" onclick=\"removeblock{$v->id}.submit();\">
                        <form id=\"removeblock{$v->id}\" name=\"removeblock{$v->id}\" method=\"post\"/>
                          <input type=\"hidden\" name=\"removeblock\" value=\"{$v->id}\" />
                        </form>
                  </span>

                  <span class=\"edit\" onclick=\"editblock{$v->id}.submit();\">
                      <form id=\"editblock{$v->id}\" name=\"editblock{$v->id}\" method=\"post\"/>
                          <input type=\"hidden\" name=\"editblock\" value=\"{$v->id}\" />
                      </form>
                  </span>
                  <span class=\"activate\">
                    " . $active . "
                  </span>
                " . '</li>';
            }
            $form .= '<ul class="slides" id="homepageblocks">' . $li . '</ul>';
        }
        if (Configuration::get('hpp_lasttab') == 3) {
            $url = '';
            $title = '';
            $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
            foreach ($languages = Language::getLanguages(false) as $language) {
                $title .= '<div id="hppname_' . $language['id_lang'] . '" style=" display: ' . ($language['id_lang'] == $id_lang_default ? 'block' : 'none') . '; "><input type="text" id="name_' . $language['id_lang'] . '" name="name[' . $language['id_lang'] . ']"></div>';
                $url .= '<div id="hppurl_' . $language['id_lang'] . '" style=" display: ' . ($language['id_lang'] == $id_lang_default ? 'block' : 'none') . '; "><input type="text" id="url_' . $language['id_lang'] . '" name="url[' . $language['id_lang'] . ']"></div>';
            }
            $title .= '<div class="flags_block">' . $this->displayFlags($languages, $id_lang_default, 'hppname', 'hppname', true) . '</div>';
            $url .= '<div class="flags_block">' . $this->displayFlags($languages, $id_lang_default, 'hppurl', 'hppurl', true) . '</div>';

            $custom_before = '';
            $c_bl = '';
            $c_al = '';
            $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
            foreach ($languages = Language::getLanguages(false) as $language) {
                $custom_before .= '<div id="hppcustombefore_' . $language['id_lang'] . '" style="width:100%; margin-bottom:10px; clear:both; display: ' . ($language['id_lang'] == $id_lang_default ? 'block' : 'none') . '; float: left;"><textarea id="custombefore_' . $language['id_lang'] . '" class="rte rtepro" name="custombefore[' . $language['id_lang'] . ']"></textarea></div>';
                $c_bl .= '<div id="hppcbl_' . $language['id_lang'] . '" style="display: ' . ($language['id_lang'] == $id_lang_default ? 'block' : 'none') . '; float: left;"><textarea id="cbl_' . $language['id_lang'] . '" class="rte rtepro" name="cbl[' . $language['id_lang'] . ']"></textarea></div>';
                $c_al .= '<div id="hppcal_' . $language['id_lang'] . '" style="display: ' . ($language['id_lang'] == $id_lang_default ? 'block' : 'none') . '; float: left;"><textarea id="cal_' . $language['id_lang'] . '" class="rte rtepro" name="cal[' . $language['id_lang'] . ']"></textarea></div>';
            }
            $custom_before .= '<div class="flags_block">' . $this->displayFlags($languages, $id_lang_default, 'hppcustombefore', 'hppcustombefore', true) . '</div>';
            $c_bl .= '<div class="flags_block">' . $this->displayFlags($languages, $id_lang_default, 'hppcbl', 'hppcbl', true) . '</div>';
            $c_al .= '<div class="flags_block">' . $this->displayFlags($languages, $id_lang_default, 'hppcal', 'hppcal', true) . '</div>';

            $languages = Language::getLanguages(true);
            $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
            global $cookie;
            $iso = Language::getIsoById((int)($cookie->id_lang));
            $isoTinyMCE = (file_exists(_PS_ROOT_DIR_ . '/js/tiny_mce/langs/' . $iso . '.js') ? $iso : 'en');
            $ad = dirname($_SERVER["PHP_SELF"]);

            $form .= (Configuration::get('hpp_rich_text') == 1 ? '<script type="text/javascript" src="' . __PS_BASE_URI__ . 'js/tiny_mce/tiny_mce.js"></script>
                        <script>
                            var iso = \'' . $isoTinyMCE . '\' ;
                            var pathCSS = \'' . _THEME_CSS_DIR_ . '\' ;
                            var ad = \'' . $ad . '\';
                        </script>
                        <script type="text/javascript" src="../modules/hpp/js/tinymce16-force-urls.inc.js"></script>' : '');
            $form .= '<fieldset>
            <legend><img src="' . $this->_path . 'logo.gif" alt="" title="" />' . $this->l('Block specification') . '</legend>
            <form name="globalsettings" id="globalsettings" method="post">

            <h3>' . $this->l('Appearance settings') . '</h3>
            <div style="padding:10px; margin:10px 0px; border:1px solid #cecece; background:#fefefe; display:block; clear:both;">
                <label>' . $this->l('Active') . '</label>
            			 <div class="margin-form">
                    <select type="text" name="hpp_active" style="max-width:200px;">
                        <option value="0" >' . $this->l('No') . '</option>
                        <option value="1" >' . $this->l('Yes') . '</option>
                    </select>
           				 </div>
                <label>' . $this->l('Method of appearing') . '</label>
            			 <div class="margin-form">
                    <select type="text" name="hpp_block_position" style="max-width:200px;">
                        <option value="1" >' . $this->l('Block') . '</option>
                        <option value="0" >' . $this->l('Tab') . '</option>
                        <option value="2" >' . $this->l('Left Column Block') . '</option>
                        <option value="3" >' . $this->l('Right Column Block') . '</option>
                    </select>
           				</div>
            </div>

            <h3>' . $this->l('Title') . '</h3>
            <div style="padding:10px; margin:10px 0px; border:1px solid #cecece; background:#fefefe; display:block; clear:both;">
                <label>' . $this->l('Name of block / tab') . '</label>
                <div class="margin-form" style="">' . $title . '</div>
               	<label>' . $this->l('Add link to name of block') . '</label>
                <div class="margin-form">
                    <select name="hpp_head_url">
                        <option value="0">' . $this->l('No') . '</option>
                        <option value="1">' . $this->l('Yes') . '</option>
                    </select>
               	</div>
                <label>' . $this->l('Link (url)') . '</label>
            			 <div class="margin-form" style="">' . $url . '</div>
            </div>


            <h3>' . $this->l('Products') . '</h3>
            <div style="padding:10px; margin:10px 0px; border:1px solid #cecece; background:#fefefe; display:block; clear:both;">
                <label>' . $this->l('What to display?') . '</label>
            			 <div class="margin-form">
                    <select type="text" name="hpp_type" id="hpp_type" style="max-width:200px;" onchange="if($(\'#hpp_type\').val()==5){$(\'#hpp_type_nb\').hide(); $(\'#hpp_type_products\').show();} else {$(\'#hpp_type_products\').hide(); $(\'#hpp_type_nb\').show();} if($(\'#hpp_type\').val()==1){$(\'#hpp_type_category\').show();}else{$(\'#hpp_type_category\').hide();}">
                        <option value="1" >' . $this->l('Products from category') . '</option>
                        <option value="2" >' . $this->l('New products') . '</option>
                        <option value="3" >' . $this->l('Best sellers') . '</option>
                        <option value="4" >' . $this->l('Specials') . '</option>
                        <option value="5" >' . $this->l('Selected products') . '</option>
                    </select>
           				 </div>
                <div id="hpp_type_category">
                    <label>' . $this->l('Category to display') . '</label>
                			 <div class="margin-form">
                        <input type="text" name="hpp_value" style="max-width:200px;"> ' . $this->l('Type here ID of category you want to display. Read') . ' <a href="http://mypresta.eu/en/art/basic-tutorials/prestashop-how-to-get-category-id.html" target="_blank">' . $this->l('how to get Category ID') . '</a>
               				 </div>
               				 <label>' . $this->l('Random order') . '</label>
                			 <div class="margin-form">
                       <select name="hpp_random">
                           <option value="0">' . $this->l('No') . '</option>
                           <option value="1">' . $this->l('Yes') . '</option>
                       </select>
                    </div>
                </div>
                <div id="hpp_type_products" style="display:none;">
                    <label>' . $this->l('Products') . '</label>
                			 <div class="margin-form">
                        <textarea name="hpp_products" class="hpp_products"></textarea>
                        <p class=""><br/>' . $this->l('enter above ID numbers of products you want to display, or search for products below.') . ' <a href="http://mypresta.eu/en/art/basic-tutorials/how-to-get-product-id-in-prestashop.html" target="_blank">' . $this->l('How to get product ID number') . '</a></p>
               				 </div>
                    <label>' . $this->l('Search for product') . '</label>
                    <div class="margin-form">
                        <input type="text" name="hpp_search" class="hpp_search" style="max-width:200px;"> ' . $this->l('') . '
                        <div id="hpp_search_result" style="margin-top:10px;">
                        </div>
                    </div>
                </div>
                <div id="hpp_type_nb">
                    <label>' . $this->l('Number of products you want to display') . '</label>
                			 <div class="margin-form">
                        <input type="text" name="hpp_nb" style="max-width:200px;">
               				 </div>
                </div>
            </div>

            <div style=' . ($this->psversion() != 6 && $this->psversion() != 7 ? 'display:none;' : '') . '>
                <h3>' . $this->l('Carousell') . '</h3>
                <div style="padding:10px; margin:10px 0px; border:1px solid #cecece; background:#fefefe; display:block; clear:both;">
                    <div>
                        <label>' . $this->l('Carousell') . '</label>
                        <div class="margin-form">
                            <select type="text" name="hpp_carousell" style="max-width:200px;">
                                <option value="0" >' . $this->l('No') . '</option>
                                <option value="1" >' . $this->l('Yes') . '</option>
                            </select><br/>' . $this->l('This feature requires default PrestaShop carousell script: BxSlider. If your theme doesnt support it, please go to module global settings and enable library.  Carousell works only in Blocks (not tabs)') . '
                       </div>
                    </div>
                    <div>
                        <label>' . $this->l('Autoplay') . '</label>
                        <div class="margin-form">
                            <select type="text" name="hpp_carousell_auto" style="max-width:200px;">
                                <option value="0" >' . $this->l('No') . '</option>
                                <option value="1" >' . $this->l('Yes') . '</option>
                            </select><br/>' . $this->l('Autostart the carousell, this option will play carousell automatically') . '
                        </div>
                    </div>
                    <label>' . $this->l('Number of products') . '</label>
                			 <div class="margin-form">
                        <input type="text" name="hpp_carousell_nb" style="max-width:200px;"><br/>
                        ' . $this->l('Number of products that will appear as a base of the carousell') . '
               				 </div>
                    <label>' . $this->l('Number of products on mobile devices') . '</label>
                			 <div class="margin-form">
                        <input type="text" name="hpp_carousell_nb_mobile" style="max-width:200px;"><br/>
                        ' . $this->l('Number of products that will appear as a base of the carousell on mobile devices') . '
               				 </div>
                </div>
            </div>

            <h3>' . $this->l('Custom contents') . '</h3>
            <div style="padding:10px; margin:10px 0px; border:1px solid #cecece; background:#fefefe; display:block; clear:both;">
               <label>' . $this->l('First custom code element') . '</label>
            			<div class="margin-form">
                   <select type="text" id="hpp_before" name="hpp_before" style="max-width:200px;" onchange="if($(\'#hpp_before\').val()==1){$(\'#customcodebefore\').show();}else{$(\'#customcodebefore\').hide();}">
                       <option value="0" >' . $this->l('No') . '</option>
                       <option value="1" >' . $this->l('Yes') . '</option>
                   </select>
                   <p class="">' . $this->l('Turn this option on if you want to add custom code block as first element of list') . '</p>
                   <p class="">' . $this->l('This option works only with one row of products') . '</p>
           				</div>
               <div id="customcodebefore" style="display:none;">
                <label>' . $this->l('Custom code') . '</label>
                <div class="margin-form">
                    ' . $custom_before . '
                    <p class="">' . $this->l('Enter custom html code above') . '</p>
                    <p class="">' . $this->l('Example of code for bootstrap templates.') . '</p>
                    <textarea style="resize:none; box-shadow: 0 1px 2px rgba(0,0,0,0.0) inset; background:none; border:0px; width:300px; height:50px;"><li class="ajax_block_product col-xs-12 col-sm-4 col-md-3">' . $this->l('Example of custom contents') . '</li></textarea>
                    </div>
               </div>

               <label>' . $this->l('Custom contents before list of products') . '</label>
            			<div class="margin-form">
                   <select type="text" id="hpp_c_bl" name="hpp_c_bl" style="max-width:200px;" onchange="if($(\'#hpp_c_bl\').val()==1){$(\'#custom_bl\').show();}else{$(\'#custom_bl\').hide();}">
                       <option value="0" >' . $this->l('No') . '</option>
                       <option value="1" >' . $this->l('Yes') . '</option>
                   </select>
                   <p class="">' . $this->l('Turn this option on if you want to add custom contents before the list of products') . '</p>
           				</div>
               <div id="custom_bl" style="display:none;">
                <label>' . $this->l('Custom code') . '</label>
                <div class="margin-form">' . $c_bl . '
                    <p class="">' . $this->l('Enter custom html code above') . '</p>
                </div>
               </div>


               <label>' . $this->l('Custom contents after list of products') . '</label>
            			<div class="margin-form">
                   <select type="text" id="hpp_c_al" name="hpp_c_al" style="max-width:200px;" onchange="if($(\'#hpp_c_al\').val()==1){$(\'#custom_al\').show();}else{$(\'#custom_al\').hide();}">
                       <option value="0" >' . $this->l('No') . '</option>
                       <option value="1" >' . $this->l('Yes') . '</option>
                   </select>
                   <p class="">' . $this->l('Turn this option on if you want to add custom contents after the list of products') . '</p>
           				</div>
               <div id="custom_al" style="display:none;">
                <label>' . $this->l('Custom code') . '</label>
                <div class="margin-form">' . $c_al . '
                    <p class="">' . $this->l('Enter custom html code above') . '</p>
                </div>
               </div>
            </div>
            <input type="hidden" name="selecttab" value="1">
            <input type="submit" class="extra button" name="add_new_block" value="' . $this->l('Add new block') . ' "/>
            </form>
            </fieldset>';
        }

        if (Configuration::get('hpp_lasttab') == 5) {
            $form= "<div class='bootstrap'>".$this->checkforupdates(0,1)."</div>";
        }

        if (Configuration::get('hpp_lasttab') == 99) {
            $form .= '
                <fieldset>
                    <legend><img src="' . $this->_path . 'logo.gif" alt="" title="" />' . $this->l('Global Settings') . '</legend>
                    <form name="globalsettings" id="globalsettings" method="post">
                        <div class="bootstrap">
	                        <div class="alert alert-info">
                            ' . $this->l('Turn this option on only if your theme doesnt have homepage tabs feature') . '
                         </div>
                         <div class="alert alert-danger">
                            ' . $this->l('This option is for PrestaShop 1.5.x only') . '
                         </div>
						                  </div>
                        <label>' . $this->l('Use module TABS instead of default prestashop tabs') . '</label>
            			         <div class="margin-form">
                            <select type="text" name="hpp_internal_tabs" style="max-width:200px;">
                                <option value="1" ' . (Configuration::get('hpp_internal_tabs') == 1 ? "selected=\"selected\" " : "") . '>' . $this->l('Yes') . '</option>
                                <option value="0" ' . (Configuration::get('hpp_internal_tabs') == 1 ? "" : "selected=\"selected\" ") . '>' . $this->l('No') . '</option>
                            </select>
           				         </div>
                        <div class="bootstrap" style="margin-top:20px;">
	                        <div class="alert alert-info">
                            ' . $this->l('Turn this option on only if you want to use tinyMCE rich text editor for custom content elements') . '
                         </div>
                        </div>
                        <label>' . $this->l('Use rich text editor') . '</label>
                        <div class="margin-form">
                            <select type="text" name="hpp_rich_text" style="max-width:200px;">
                                <option value="1" ' . (Configuration::get('hpp_rich_text') == 1 ? "selected=\"selected\" " : "") . '>' . $this->l('Yes') . '</option>
                                <option value="0" ' . (Configuration::get('hpp_rich_text') == 1 ? "" : "selected=\"selected\" ") . '>' . $this->l('No') . '</option>
                            </select>
           				         </div>
                        <div style=' . ($this->psversion() != 6 && $this->psversion() != 7 ? 'display:none;' : '') . '>
                            <div class="bootstrap" style="margin-top:20px;">
    	                        <div class="alert alert-info">
                              ' . $this->l('If you want to use carousell slider for blocks and if your theme doesnt support bxslider - use this option to load bxslider library') . '
                             </div>
    						                  </div>
                            <label>' . $this->l('Load BxSlider') . '</label>
                            <div class="margin-form">
                                <select type="text" name="hpp_bxslider" style="max-width:200px;">
                                    <option value="1" ' . (Configuration::get('hpp_bxslider') == 1 ? "selected=\"selected\" " : "") . '>' . $this->l('Yes') . '</option>
                                    <option value="0" ' . (Configuration::get('hpp_bxslider') == 1 ? "" : "selected=\"selected\" ") . '>' . $this->l('No') . '</option>
                                </select>
               				         </div>
                        </div>
                        <input type="submit" class="extra button" name="global_settings" value="' . $this->l('save settings') . ' "/>
                     </form>
                 </fieldset>';
        }

        return $output . '
        <script type="text/javascript" src="../modules/hpp/js/script.js"/></script>
        <script type="text/javascript">
    			 $(function() {
    				var $mySlides = $("#homepageblocks");
    				$mySlides.sortable({
    					opacity: 0.6,
    					cursor: "move",
    					update: function() {
    						var order = $(this).sortable("serialize") + "&action=updateSlidesPosition";
    						$.post("../modules/' . $this->name . '/ajax_' . $this->name . '.php", order);
    						}
    					});
    				$mySlides.hover(function() {
    					$(this).css("cursor","move");
    					},
    					function() {
    					$(this).css("cursor","auto");
    				});
    			});
    		    </script>
        <style>
            .language_flags {text-align:left;}
            #topmenu-horizontal-module {overflow:hidden; background-color: #F8F8F8; border: 1px solid #CCCCCC; margin-bottom: 10px; padding: 10px 0; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;}
            #topmenu-horizontal-module .addnew, #topmenu-horizontal-module .btnpro {-webkit-border-radius:4px; -moz-border-radius:4px; -border-radius:4px; padding:5px; margin-right:10px; cursor:pointer; width:52px; height:52px; display:inline-block; float:right; text-align:center; border:1px dotted #c0c0c0; }
            #topmenu-horizontal-module .addnew:hover, #topmenu-horizontal-module .btnpro:hover {border:1px solid #bfbfbf; background:#f3f3f3;}
            #topmenu-horizontal-module span.img {margin:auto; width:32px; height:32px; display:block;}
            #topmenu-horizontal-module span.txt {margin-top:3px; width:52px; display:block; text-align:center;}
            #topmenu-horizontal-module .addnew span.img {background:url(\'' . _MODULE_DIR_ . $this->name . '/img/add.png\') no-repeat center;}
            #topmenu-horizontal-module .save span.img {background:url(\'' . _MODULE_DIR_ . $this->name . '/img/on.png\') no-repeat center;}
            #topmenu-horizontal-module .back span.img {background:url(\'' . _MODULE_DIR_ . $this->name . '/img/back.png\') no-repeat center;}

                .slides {margin:0px; padding:0px;}
                .slides li { font-size:15px!important; list-style: none; margin: 0 0 4px 0; padding: 15px 10px; background-color: #F4E6C9; border: #CCCCCC solid 1px; color:#000;}
                .slides li:hover {border:1px #000 dashed; cursor:move;}
                .slides li .name {font-size:18px!important;}
                .slides li .nb {color:#FFF; background:#000; padding:5px 10px; font-size:18px; font-weight:bold; margin-right:10px; }

                .activate {display:inline-block; float:right; padding-right:5px;  cursor:pointer; position:relative; top:2px;}
                .activate img {max-width:50px; height:auto;}
                .remove {opacity:0.3; position:relative; top:-1px; width:24px; height:24px; display:inline-block; float:right; background:url("../modules/' . $this->name . '/img/trash.png") top no-repeat; cursor:pointer;}
                .edit {margin-right:6px; opacity:0.3; position:relative;  width:24px; height:24px; display:inline-block; float:right; background:url("../modules/' . $this->name . '/img/edit.png") top no-repeat; cursor:pointer;}

                .remove:hover, .edit:hover, .activate:hover { opacity:1.0; }
                .edit,.remove {margin-right:5px;}


        </style>
        <form name="selectform1" id="selectform1" action="' . $_SERVER['REQUEST_URI'] . '" method="post"><input type="hidden" name="selecttab" value="1"></form>
        <form name="selectform2" id="selectform2" action="' . $_SERVER['REQUEST_URI'] . '" method="post"><input type="hidden" name="selecttab" value="2"></form>
        <form name="selectform3" id="selectform3" action="' . $_SERVER['REQUEST_URI'] . '" method="post"><input type="hidden" name="selecttab" value="3"></form>
        <form name="selectform4" id="selectform4" action="' . $_SERVER['REQUEST_URI'] . '" method="post"><input type="hidden" name="selecttab" value="4"></form>
        <form name="selectform99" id="selectform99" action="' . $_SERVER['REQUEST_URI'] . '" method="post"><input type="hidden" name="selecttab" value="99"></form>

        <form name="selectform5" id="selectform5" action="' . $_SERVER['REQUEST_URI'] . '" method="post"><input type="hidden" name="selecttab" value="5"></form>
        ' . "<div id='cssmenu'>
            <ul>
               <li class='bgver'><a><span>v" . $this->version . "</span></a></li>
               <li class='" . (Configuration::get('hpp_lasttab') == 99 ? 'active' : '') . "'><a href='#' onclick=\"selectform99.submit()\"><span>" . $this->l('Global Settings') . "</span></a></li>
               <li class='" . (Configuration::get('hpp_lasttab') == 1 ? 'active' : '') . "'><a href='#' onclick=\"selectform1.submit()\"><span>" . $this->l('Blocks') . "</span></a></li>
               <li class='" . (Configuration::get('hpp_lasttab') == 3 ? 'active' : '') . "'><a href='#' onclick=\"selectform3.submit()\"><span>" . $this->l('Add new') . "</span></a></li>
               <li style='position:relative; display:inline-block; float:right; '><a style=\"display:block; width:40px;\" href='https://mypresta.eu' target='_blank' title='prestashop modules'><span ><img src='../modules/hpp/img/logo-white.png' alt='prestashop modules' style=\"position:absolute; top:17px; right:16px;\"/></span></a></li>
               <li style='float:right; display:inline-block;' class='" . (Configuration::get('hpp_lasttab') == 5 ? 'active' : '') . "'><a href='#' onclick=\"selectform5.submit()\"><span>" . $this->l('Updates') . "</span></a></li>
            </ul>
        </div>" . '<link href="../modules/' . $this->name . '/css.css" rel="stylesheet" type="text/css" />' . $form;
    }

    public function inconsistency()
    {
        $prefix = _DB_PREFIX_;
        $engine = _MYSQL_ENGINE_;
        $table['hpp_block']['list_products']['type'] = 'text';
        $table['hpp_block']['list_products']['length'] = 'X';
        $table['hpp_block']['list_products']['default'] = 'X';
        $table['hpp_block']['before']['type'] = 'int';
        $table['hpp_block']['before']['length'] = '1';
        $table['hpp_block']['before']['default'] = '0';
        $table['hpp_block_lang']['custom_before']['type'] = 'text';
        $table['hpp_block_lang']['custom_before']['length'] = 'X';
        $table['hpp_block_lang']['custom_before']['default'] = 'X';
        $table['hpp_block']['after']['type'] = 'int';
        $table['hpp_block']['after']['length'] = '1';
        $table['hpp_block']['after']['default'] = '0';
        $table['hpp_block']['random']['type'] = 'int';
        $table['hpp_block']['random']['length'] = '1';
        $table['hpp_block']['random']['default'] = '0';
        $table['hpp_block_lang']['custom_after']['type'] = 'text';
        $table['hpp_block_lang']['custom_after']['length'] = 'X';
        $table['hpp_block_lang']['custom_after']['default'] = 'X';
        $table['hpp_block']['head_url']['type'] = 'int';
        $table['hpp_block']['head_url']['length'] = '1';
        $table['hpp_block']['head_url']['default'] = '0';
        $table['hpp_block_lang']['url']['type'] = 'text';
        $table['hpp_block_lang']['url']['length'] = 'X';
        $table['hpp_block_lang']['url']['default'] = 'X';
        $table['hpp_block']['carousell']['type'] = 'int';
        $table['hpp_block']['carousell']['length'] = '1';
        $table['hpp_block']['carousell']['default'] = '0';
        //1.6.4
        $table['hpp_block_lang']['custom_bl']['type'] = 'text';
        $table['hpp_block_lang']['custom_bl']['length'] = 'X';
        $table['hpp_block_lang']['custom_bl']['default'] = 'X';
        $table['hpp_block_lang']['custom_al']['type'] = 'text';
        $table['hpp_block_lang']['custom_al']['length'] = 'X';
        $table['hpp_block_lang']['custom_al']['default'] = 'X';
        $table['hpp_block']['c_al']['type'] = 'int';
        $table['hpp_block']['c_al']['length'] = '1';
        $table['hpp_block']['c_al']['default'] = '0';
        $table['hpp_block']['c_bl']['type'] = 'int';
        $table['hpp_block']['c_bl']['length'] = '1';
        $table['hpp_block']['c_bl']['default'] = '0';
        //1.6.5
        $table['hpp_block']['carousell_auto']['type'] = 'int';
        $table['hpp_block']['carousell_auto']['length'] = '1';
        $table['hpp_block']['carousell_auto']['default'] = '0';
        $table['hpp_block']['carousell_nb']['type'] = 'int';
        $table['hpp_block']['carousell_nb']['length'] = '1';
        $table['hpp_block']['carousell_nb']['default'] = '0';
        //1.6.6
        $table['hpp_block']['carousell_nb_mobile']['type'] = 'int';
        $table['hpp_block']['carousell_nb_mobile']['length'] = '1';
        $table['hpp_block']['carousell_nb_mobile']['default'] = '1';

        $return = '';

        //hpp_block
        foreach (Db::getInstance()->executeS("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA ='" . _DB_NAME_ . "' AND TABLE_NAME='" . _DB_PREFIX_ . "hpp_block'") AS $key => $column) {
            $return[$column['COLUMN_NAME']] = "1";
        }
        foreach ($table['hpp_block'] as $key => $field) {
            if (!isset($return[$key])) {
                $error[$key]['type'] = "0";
                $error[$key]['message'] = $this->l('Database inconsistency, column does not exist');
                if ($field['default'] != "X") {
                    if ($this->runStatement("ALTER TABLE `${prefix}hpp_block` ADD COLUMN `" . $key . "` " . $field['type'] . "(" . $field['length'] . ") NULL DEFAULT '" . $field['default'] . "'")) {
                        $error[$key]['fixed'] = $this->l('... FIXED!');
                    } else {
                        $error[$key]['fixed'] = $this->l('... ERROR!');
                    }
                } else {
                    if ($this->runStatement("ALTER TABLE `${prefix}hpp_block` ADD COLUMN `" . $key . "` " . $field['type'])) {
                        $error[$key]['fixed'] = $this->l('... FIXED!');
                    } else {
                        $error[$key]['fixed'] = $this->l('... ERROR!');
                    }
                }
                if (isset($field['config'])) {
                    Configuration::updateValue($field['config'], '1');
                }
            } else {
                $error[$key]['type'] = "1";
                $error[$key]['message'] = $this->l('OK!');
                $error[$key]['fixed'] = $this->l('');
                if (isset($field['config'])) {
                    Configuration::updateValue($field['config'], '1');
                }
            }
        }


        //hpp_block_lang
        foreach (Db::getInstance()->executeS("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA ='" . _DB_NAME_ . "' AND TABLE_NAME='" . _DB_PREFIX_ . "hpp_block_lang'") AS $key => $column) {
            $return[$column['COLUMN_NAME']] = "1";
        }
        foreach ($table['hpp_block_lang'] as $key => $field) {
            if (!isset($return[$key])) {
                $error[$key]['type'] = "0";
                $error[$key]['message'] = $this->l('Database inconsistency, column does not exist');
                if ($field['default'] != "X") {
                    if ($this->runStatement("ALTER TABLE `${prefix}hpp_block_lang` ADD COLUMN `" . $key . "` " . $field['type'] . "(" . $field['length'] . ") NULL DEFAULT '" . $field['default'] . "'")) {
                        $error[$key]['fixed'] = $this->l('... FIXED!');
                    } else {
                        $error[$key]['fixed'] = $this->l('... ERROR!');
                    }
                } else {
                    if ($this->runStatement("ALTER TABLE `${prefix}hpp_block_lang` ADD COLUMN `" . $key . "` " . $field['type'])) {
                        $error[$key]['fixed'] = $this->l('... FIXED!');
                    } else {
                        $error[$key]['fixed'] = $this->l('... ERROR!');
                    }
                }
                if (isset($field['config'])) {
                    Configuration::updateValue($field['config'], '1');
                }
            } else {
                $error[$key]['type'] = "1";
                $error[$key]['message'] = $this->l('OK!');
                $error[$key]['fixed'] = $this->l('');
                if (isset($field['config'])) {
                    Configuration::updateValue($field['config'], '1');
                }
            }
        }
        $form .= '<table class="inconsistency"><tr><td colspan="4" style="text-align:center">' . $this->l('UPGRADE') . '</td></tr>';
        foreach ($error as $column => $info) {
            $form .= "<tr><td class='inconsistency" . $info['type'] . "'></td><td>" . $column . "</td><td>" . $info['message'] . "</td><td>" . $info['fixed'] . "</td></tr>";
        }
        $form .= "</table>";

        //return $form;
        return true;
    }

}


class hppUpdate extends hpp
{
    public static function version($version)
    {
        $version = (int)str_replace(".", "", $version);
        if (strlen($version) == 3) {
            $version = (int)$version . "0";
        }
        if (strlen($version) == 2) {
            $version = (int)$version . "00";
        }
        if (strlen($version) == 1) {
            $version = (int)$version . "000";
        }
        if (strlen($version) == 0) {
            $version = (int)$version . "0000";
        }
        return (int)$version;
    }

    public static function encrypt($string)
    {
        return base64_encode($string);
    }

    public static function verify($module, $key, $version)
    {
        if (ini_get("allow_url_fopen")) {
            if (function_exists("file_get_contents")) {
                $actual_version = @file_get_contents('http://dev.mypresta.eu/update/get.php?module=' . $module . "&version=" . self::encrypt($version) . "&lic=$key&u=" . self::encrypt(_PS_BASE_URL_ . __PS_BASE_URI__));
            }
        }
        Configuration::updateValue("update_" . $module, date("U"));
        Configuration::updateValue("updatev_" . $module, $actual_version);
        return $actual_version;
    }
}

?>