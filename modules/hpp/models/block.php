<?php

/**
 * PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
 *
 * @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
 * @copyright 2010-2016 VEKIA
 * @license   This program is not free software and you can't resell and redistribute it
 *
 * CONTACT WITH DEVELOPER http://mypresta.eu
 * support@mypresta.eu
 */
class HppBlock extends ObjectModel
{
    public $id;
    public $type;
    public $active;
    public $block_position;
    public $position;
    public $value;
    public $shop;
    public $name;
    public $nb;
    public $list_products;
    public $custom_before;
    public $before;
    public $random;
    public $carousell;
    public $carousell_nb;
    public $carousell_nb_mobile;
    public $carousell_auto;
    public $head_url;
    public $url;
    public $custom_bl;
    public $custom_al;
    public $c_al;
    public $c_bl;

    public static $definition = array(
        'table' => 'hpp_block',
        'primary' => 'id',
        'multilang' => true,
        'fields' => array(
            'id' => array('type' => ObjectModel::TYPE_INT),
            'type' => array('type' => ObjectModel::TYPE_INT, 'required' => true),
            'block_position' => array('type' => ObjectModel::TYPE_INT),
            'active' => array('type' => ObjectModel::TYPE_BOOL),
            'carousell' => array('type' => ObjectModel::TYPE_BOOL),
            'carousell_nb' => array('type' => ObjectModel::TYPE_INT),
            'carousell_nb_mobile' => array('type' => ObjectModel::TYPE_INT),
            'carousell_auto' => array('type' => ObjectModel::TYPE_BOOL),
            'random' => array('type' => ObjectModel::TYPE_INT),
            'position' => array('type' => ObjectModel::TYPE_INT),
            'shop' => array('type' => ObjectModel::TYPE_INT),
            'value' => array('type' => ObjectModel::TYPE_STRING, 'size' => 254),
            'name' => array('type' => ObjectModel::TYPE_STRING, 'lang' => true, 'size' => 254),
            'nb' => array('type' => ObjectModel::TYPE_INT),
            'list_products' => array('type' => ObjectModel::TYPE_STRING),
            'before' => array('type' => ObjectModel::TYPE_BOOL),
            'custom_before' => array('type' => ObjectModel::TYPE_HTML, 'lang' => true),
            'custom_bl' => array('type' => ObjectModel::TYPE_HTML, 'lang' => true),
            'custom_al' => array('type' => ObjectModel::TYPE_HTML, 'lang' => true),
            'c_al' => array('type' => ObjectModel::TYPE_INT),
            'c_bl' => array('type' => ObjectModel::TYPE_INT),
            'url' => array('type' => ObjectModel::TYPE_HTML, 'lang' => true),
            'head_url' => array('type' => ObjectModel::TYPE_INT),
        ),
    );

    public function __construct($id = NULL)
    {
        parent::__construct($id);
    }

    public static function psversion($part = 1)
    {
        $version = _PS_VERSION_;
        $exp = explode('.', $version);
        if ($part == 1) {
            return $exp[1];
        }
        if ($part == 2) {
            return $exp[2];
        }
        if ($part == 3) {
            return $exp[3];
        }
    }

    public static function getAllBlocks()
    {
        $context = new Context();
        $context = $context->getContext();
        if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') == 1) {
            $whereshop = 'WHERE shop="'.$context->shop->id.'"';
        }
        else {
            $whereshop = '';
        }

        $id_lang = context::getContext()->cookie->id_lang;
        $query = Db::getInstance()->ExecuteS('SELECT id FROM `'._DB_PREFIX_.'hpp_block` '.(string)$whereshop.' ORDER BY `position` ');
        $blocks = array();
        foreach ($query as $key) {
            $blocks[$key['id']] = new hppblock($key['id']);
            $blocks[$key['id']]->custom_al = $blocks[$key['id']]->custom_al["$id_lang"];
            $blocks[$key['id']]->custom_bl = $blocks[$key['id']]->custom_bl["$id_lang"];
            $blocks[$key['id']]->name = $blocks[$key['id']]->name["$id_lang"];
            $blocks[$key['id']]->url = $blocks[$key['id']]->url["$id_lang"];
            $blocks[$key['id']]->custom_before = $blocks[$key['id']]->custom_before["$id_lang"];
        }
        return $blocks;
    }

    public static function getAllBlocksByPosition($position)
    {
        $context = new Context;
        $context = $context->getContext();
        $whereshop = '';
        if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') == 1) {
            $whereshop = 'AND shop="'.$context->shop->id.'"';
        }

        $id_lang = context::getContext()->cookie->id_lang;
        $query = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'hpp_block` WHERE block_position='.(string)$position.' AND active=1 '.(string)$whereshop.' ORDER BY position');
        $blocks = array();
        foreach ($query as $key) {
            $blocks[$key['id']] = new hppblock($key['id']);
            $blocks[$key['id']]->name = $blocks[$key['id']]->name["$id_lang"];
            $blocks[$key['id']]->custom_al = $blocks[$key['id']]->custom_al["$id_lang"];
            $blocks[$key['id']]->custom_bl = $blocks[$key['id']]->custom_bl["$id_lang"];
            $blocks[$key['id']]->url = $blocks[$key['id']]->url["$id_lang"];
            $blocks[$key['id']]->custom_before = $blocks[$key['id']]->custom_before["$id_lang"];
        }
        return $blocks;

    }

}
