{**
* PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
*
* @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
* @copyright 2010-2015 VEKIA
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER http://mypresta.eu
* support@mypresta.eu
*}

{foreach from=$blocks item=block}
    <div id="hppContainer{$block->id|escape:'int':'utf-8'}" class="hppContainer  hppColumn block products_block clearfix">
        <h4 class="title_block">{$block->name|escape:'html':'utf-8'}</h4>

		    {* CUSTOM CODE BEFORE LIST OF PRODUCTS *}
		    {if $block->c_bl==1}
			    <div class="row">
				    <div class="col-lg-12 custom_code_before" id="custom_code_before{$block->id}">
					    {$block->custom_bl}
				    </div>
			    </div>
		    {/if}
		    {* CUSTOM CODE AFTER LIST OF PRODUCTS *}

            {if isset($block->products) && $block->products}
                {assign var='liHeight' value=250}
    			{assign var='nbItemsPerLine' value=4}
    			{assign var='nbLi' value=$block->products|@count}
    			{math equation="nbLi/nbItemsPerLine" nbLi=$nbLi nbItemsPerLine=$nbItemsPerLine assign=nbLines}
    			{math equation="nbLines*liHeight" nbLines=$nbLines|ceil liHeight=$liHeight assign=ulHeight}
    			<ul id="hpp{$block->id|escape:'int':'utf-8'}" style="height:{$ulHeight|escape:'int':'utf-8'}px;">
    			{foreach from=$block->products item=product name=homeFeaturedProducts}
    				{math equation="(total%perLine)" total=$smarty.foreach.homeFeaturedProducts.total perLine=$nbItemsPerLine assign=totModulo}
    				{if $totModulo == 0}{assign var='totModulo' value=$nbItemsPerLine}{/if}
    				<li class="ajax_block_product {if $smarty.foreach.homeFeaturedProducts.first}first_item{elseif $smarty.foreach.homeFeaturedProducts.last}last_item{else}item{/if} {if $smarty.foreach.homeFeaturedProducts.iteration%$nbItemsPerLine == 0}last_item_of_line{elseif $smarty.foreach.homeFeaturedProducts.iteration%$nbItemsPerLine == 1} {/if} {if $smarty.foreach.homeFeaturedProducts.iteration > ($smarty.foreach.homeFeaturedProducts.total - $totModulo)}last_line{/if}">
    					<a href="{$product.link|escape:'html'}" title="{$product.name|escape:html:'UTF-8'}" class="product_image">
                            {assign var="pImages" value=hpp::geImagesByID($product.id_product, 1)}
                            {if $pImages}
                                {foreach from=$pImages item=image name=images}
                                    <img src="{$link->getImageLink($product.link_rewrite, $image, 'home_default')}" {if $smarty.foreach.images.first}class="current img_{$smarty.foreach.images.index}"{else} class="img_{$smarty.foreach.images.index}" style="display:none;"{/if} alt="{$product.legend|escape:'htmlall':'UTF-8'}" {if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if}/>
                                {/foreach}
                            {/if}
                        {if isset($product.new) && $product.new == 1}<span class="new">{l s='New' mod='hpp'}</span>{/if}</a>
    					<h5 class="s_title_block"><a href="{$product.link|escape:'html'}" title="{$product.name|truncate:50:'...'|escape:'htmlall':'UTF-8'}">{$product.name|truncate:35:'...'|escape:'htmlall':'UTF-8'}</a></h5>
    					<div class="product_desc"><a href="{$product.link|escape:'html'}" title="{l s='More' mod='hpp'}">{$product.description_short|strip_tags|truncate:65:'...'}</a></div>
    					<div>
    						<a class="lnk_more" href="{$product.link|escape:'html'}" title="{l s='View' mod='hpp'}">{l s='View' mod='hpp'}</a>
    						{if $product.show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}<p class="price_container"><span class="price">{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}</span></p>{else}<div style="height:21px;"></div>{/if}
    						
    						{if ($product.id_product_attribute == 0 OR (isset($add_prod_display) AND ($add_prod_display == 1))) AND $product.available_for_order AND !isset($restricted_country_mode) AND $product.minimal_quantity == 1 AND $product.customizable != 2 AND !$PS_CATALOG_MODE}
    							{if ($product.quantity > 0 OR $product.allow_oosp)}
    							<a class="exclusive ajax_add_to_cart_button" rel="ajax_id_product_{$product.id_product}" href="{$link->getPageLink('cart')|escape:'html'}?qty=1&amp;id_product={$product.id_product}&amp;token={$static_token}&amp;add" title="{l s='Add to cart' mod='hpp'}">{l s='Add to cart' mod='hpp'}</a>
    							{else}
    							<span class="exclusive">{l s='Add to cart' mod='hpp'}</span>
    							{/if}
    						{else}
    							<div style="height:23px;"></div>
    						{/if}
    					</div>
    				</li>
    			{/foreach}
    			</ul>
            {else}
                <ul class="hppContainer{$block->id|escape:'int':'utf-8'}_noProducts tab-pane">
                	<li class="alert alert-info">{l s='No products at this time.' mod='hpp'}</li>
                </ul>
            {/if}
    </div>
	{* CUSTOM CODE AFTER LIST OF PRODUCTS *}
	{if $block->c_al==1}
		<div class="row">
			<div class="col-lg-12 custom_code_before" id="custom_code_before{$block->id}">
				{$block->custom_al}
			</div>
		</div>
	{/if}
	{* CUSTOM CODE AFTER LIST OF PRODUCTS *}



        {if $block->before==1}
        <div style="display:none!important;" id="hppcontents{$block->id|escape:'int':'utf-8'}">
            {$block->custom_before|escape:'html':'utf-8'}
        </div>
        <script>
            $(document).ready(function(){literal}{{/literal}
                $("#hpp{$block->id|escape:'int':'utf-8'} li").removeClass('first-in-line').removeClass('last-line').removeClass('first-item-of-tablet-line').removeClass('first-item-of-mobile-line');
                $("#hpp{$block->id|escape:'int':'utf-8'}").prepend($("#hppcontents{$block->id|escape:'int':'utf-8'}").html());
                $("#hppcontents{$block->id|escape:'int':'utf-8'}").remove();
            {literal}}{/literal});
        </script>
        {/if}
{/foreach}