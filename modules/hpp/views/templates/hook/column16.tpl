{**
* PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
*
* @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
* @copyright 2010-2015 VEKIA
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER http://mypresta.eu
* support@mypresta.eu
*}
{foreach from=$blocks item=block}
    <div id="hppContainer{$block->id|escape:'int':'utf-8'}" class="hppColumnBlock hppColumnBlock{$block->id|escape:'int':'utf-8'} block products_block clearfix">
        {if $block->head_url==1}<a href="{$block->url}">{/if}<h4 class="title_block">{$block->name|escape:'html':'utf-8'}</h4>{if $block->head_url==1}</a>{/if}
            {* CUSTOM CODE BEFORE LIST OF PRODUCTS *}
            {if $block->c_bl==1}
                <div class="row">
                    <div class="col-lg-12 custom_code_before" id="custom_code_before{$block->id}">
                        {$block->custom_bl}
                    </div>
                </div>
            {/if}
            {* CUSTOM CODE AFTER LIST OF PRODUCTS *}
            {if isset($block->products) && $block->products}
                {if $block->carousell==1}
            	   {include file="$tpl_dir./product-list.tpl" class="homefeatured{$block->id} homefeaturedslider" id="hpp{$block->id}" products=$block->products}
                       {literal}
                            <style>
                            .hppColumnBlock{/literal}{$block->id|escape:'int':'utf-8'}{literal} li {
                                display:block;
                                width:300px!important;
                            }
                        </style>
                    {/literal}
                {else}
                    {literal}
                        <style>
                            .hppColumnBlock{/literal}{$block->id|escape:'int':'utf-8'}{literal} li {
                                display:block;
                                width:100%!important;
                            }
                        </style>
                    {/literal}
                    {include file="$tpl_dir./product-list.tpl" class="homefeatured{$block->id} " id="hpp{$block->id}" products=$block->products}
                {/if}
                {if $block->carousell==1}
                    {literal}
                        <script>
                            $(document).ready(function() {
                        	if (!!$.prototype.bxSlider)
                        		$('.{/literal}homefeatured{$block->id}{literal}').bxSlider({
                                  minSlides: {/literal}{if $block->carousell_nb=="" || $block->carousell_nb==0}1{else}{$block->carousell_nb}{/if}{literal},
                                  maxSlides: {/literal}{if $block->carousell_nb=="" || $block->carousell_nb==0}1{else}{$block->carousell_nb}{/if}{literal},
                                  auto: {/literal}{if $block->carousell_auto==1}true{else}false{/if}{literal},
                        		  infiniteLoop: false, 
                                  hideControlOnEnd: true,
                                  controls: false,
                                  adaptiveHeight: true,
                                  pager: true,
                                  moveSlides:1
                                });
                            });
                        </script>
                    {/literal}
                {/if}
            {else}
                <ul class="hppContainer{$block->id|escape:'int':'utf-8'}_noProducts tab-pane">
                	<li class="alert alert-info">{l s='No products at this time.' mod='hpp'}</li>
                </ul>
            {/if}
    </div>
    {* CUSTOM CODE AFTER LIST OF PRODUCTS *}
    {if $block->c_al==1}
        <div class="row">
            <div class="col-lg-12 custom_code_before" id="custom_code_before{$block->id}">
                {$block->custom_al}
            </div>
        </div>
    {/if}
    {* CUSTOM CODE AFTER LIST OF PRODUCTS *}



    {if $block->before==1}
        <div style="display:none!important;" id="hppcontents{$block->id|escape:'int':'utf-8'}">
            {$block->custom_before|escape:'':'utf-8'}
        </div>
        <script>
            $(document).ready(function(){literal}{{/literal}
                $("#hpp{$block->id|escape:'int':'utf-8'} li").removeClass('first-in-line').removeClass('last-line').removeClass('first-item-of-tablet-line').removeClass('first-item-of-mobile-line');
                $("#hpp{$block->id|escape:'int':'utf-8'}").prepend($("#hppcontents{$block->id|escape:'int':'utf-8'}").html());
                $("#hppcontents{$block->id|escape:'int':'utf-8'}").remove();
            {literal}}{/literal});
        </script>
    {/if}
{/foreach}