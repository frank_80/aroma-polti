{**
* PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
*
* @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
* @copyright 2010-2015 VEKIA
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER http://mypresta.eu
* support@mypresta.eu
*}

{foreach from=$blocks item=block}
    <div id="hppContainer{$block->id|escape:'int':'utf-8'}" class="{if $block->carousell==1}hppContainerBlockMainDiv{/if} block products_block clearfix">
        <h4 class="title_block">{if $block->head_url==1}<a href="{$block->url}">{/if}{$block->name|escape:'html':'utf-8'}{if $block->head_url==1}</a>{/if}<span class="hppback hppb{$block->id}"></span><span class="hppf{$block->id} hppforward"></span></h4>

            {* CUSTOM CODE BEFORE LIST OF PRODUCTS *}
            {if $block->c_bl==1}
                <div class="row">
                    <div class="col-lg-12 custom_code_before" id="custom_code_before{$block->id}">
                        {$block->custom_bl}
                    </div>
                </div>
            {/if}
            {* CUSTOM CODE AFTER LIST OF PRODUCTS *}

            {if isset($block->products) && $block->products}
                {if $block->carousell==1}
            	   {include file="$tpl_dir./product-list.tpl" class="homefeatured{$block->id} tab-pane homefeaturedslider" id="hpp{$block->id}" products=$block->products}
                {else}
                   {include file="$tpl_dir./product-list.tpl" class="homefeatured{$block->id} tab-pane" id="hpp{$block->id}" products=$block->products}
                {/if}
            {else}
                <ul class="hppContainer{$block->id|escape:'int':'utf-8'}_noProducts tab-pane">
                	<li class="alert alert-info">{l s='No products at this time.' mod='hpp'}</li>
                </ul>
            {/if}

            {* CUSTOM CODE AFTER LIST OF PRODUCTS *}
            {if $block->c_al==1}
                <div class="row">
                    <div class="col-lg-12 custom_code_before" id="custom_code_before{$block->id}">
                        {$block->custom_al}
                    </div>
                </div>
            {/if}
            {* CUSTOM CODE AFTER LIST OF PRODUCTS *}
    </div>
    {if $block->carousell==1}
        {literal}
            <script>
                $(document).ready(function() {
                    var getWidth = $('#hppContainer{/literal}{$block->id|escape:'int':'utf-8'}{literal}').width();
                    if (getWidth<800 && getWidth>500){
                        var setWidth=getWidth/2;
                    } else if (getWidth>800 && getWidth<1000) {
                        var setWidth=getWidth/3;
                    } else if (getWidth<500) {
                        var setWidth=getWidth;
                    } else {
                        var setWidth=292;
                    }
            	if (!!$.prototype.bxSlider)
            		$('.{/literal}homefeatured{$block->id}{literal}').bxSlider({
            			minSlides: {/literal}{if $block->carousell_nb_mobile=="" || $block->carousell_nb==0}1{else}{$block->carousell_nb_mobile}{/if}{literal},
            			maxSlides: {/literal}{if $block->carousell_nb=="" || $block->carousell_nb==0}4{else}{$block->carousell_nb}{/if}{literal},
            			slideWidth: setWidth,
            			slideMargin: 10,
            			pager: false,
                        auto: {/literal}{if $block->carousell_auto==1}true{else}false{/if}{literal},
            			controls: true,
            			moveSlides:1,
            			infiniteLoop:false,
            			hideControlOnEnd: true,
                        nextSelector: '.hppf{/literal}{$block->id}{literal}',
                        prevSelector: '.hppb{/literal}{$block->id}{literal}'
            		});
                });
            </script>
        {/literal}
    {/if}
    
    {if $block->before==1}
        <div style="display:none!important;" id="hppcontents{$block->id|escape:'int':'utf-8'}">
            {$block->custom_before|escape:'':'utf-8'}
        </div>
        <script>
            $(document).ready(function(){literal}{{/literal}
                $("#hpp{$block->id|escape:'int':'utf-8'} li").removeClass('first-in-line').removeClass('last-line').removeClass('first-item-of-tablet-line').removeClass('first-item-of-mobile-line');
                $("#hpp{$block->id|escape:'int':'utf-8'}").prepend($("#hppcontents{$block->id|escape:'int':'utf-8'}").html());
                $("#hppcontents{$block->id|escape:'int':'utf-8'}").remove();
            {literal}}{/literal});
        </script>
    {/if}
{/foreach}