{**
* PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
*
* @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
* @copyright 2010-2015 VEKIA
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER http://mypresta.eu
* support@mypresta.eu
*}

{foreach from=$blocks item=block}

    {* CUSTOM CODE BEFORE LIST OF PRODUCTS *}
    {if $block->c_bl==1}
        <div class="row">
            <div class="col-lg-12 custom_code_before" id="custom_code_before{$block->id}">
                {$block->custom_bl}
            </div>
        </div>
    {/if}
    {* CUSTOM CODE AFTER LIST OF PRODUCTS *}

    {if isset($block->products) && $block->products}
    	{include file="$tpl_dir./product-list-tab-home.tpl" class='tab-pane' id="hppTabContainer{$block->id}" products=$block->products}
    {else}
        <ul id="hppTabContainer{$block->id|escape:'int':'utf-8'}" class="tab-pane">
        	<li class="alert alert-info">{l s='No products at this time.' mod='hpp'}</li>
        </ul>
    {/if}

    {* CUSTOM CODE AFTER LIST OF PRODUCTS *}
    {if $block->c_al==1}
        <div class="row">
            <div class="col-lg-12 custom_code_before" id="custom_code_before{$block->id}">
                {$block->custom_al}
            </div>
        </div>
    {/if}
    {* CUSTOM CODE AFTER LIST OF PRODUCTS *}

    {if $block->before==1}
        <div style="display:none!important;" id="hppcontents{$block->id|escape:'int':'utf-8'}">
            {$block->custom_before|escape:'':'utf-8'}
        </div>
        <script>
            $(document).ready(function(){literal}{{/literal}
                $("#hppTabContainer{$block->id|escape:'int':'utf-8'} li").removeClass('first-in-line').removeClass('last-line').removeClass('first-item-of-tablet-line').removeClass('first-item-of-mobile-line');
                $("#hppTabContainer{$block->id|escape:'int':'utf-8'}").prepend($("#hppcontents{$block->id|escape:'int':'utf-8'}").html());
                $("#hppcontents{$block->id|escape:'int':'utf-8'}").remove();
            {literal}}{/literal});
        </script>
    {/if}
{/foreach}