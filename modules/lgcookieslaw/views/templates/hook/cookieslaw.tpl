{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="lgcookieslaw_banner">
	<form method="post" action="#">
		{if $buttons_position == 2 }
			<input name="aceptocookies" class="lgcookieslaw_btn" type="submit" value="{stripslashes($button1|escape:'quotes':'UTF-8')}" >
			<a class="lgcookieslaw_btn" {if isset($cms_target) && $cms_target} target="_blank" {/if} href="{$cms_link|escape:'quotes':'UTF-8'}" >
				{stripslashes($button2|escape:'quotes':'UTF-8')}
			</a>
		{/if}
		<div class="lgcookieslaw_container">
			{if $buttons_position == 4}
				<div class="lgcookieslaw_button_container" style="padding:5px">
					<input name="aceptocookies" class="lgcookieslaw_btn" type="submit" value="{stripslashes($button1|escape:'quotes':'UTF-8')}" >
					<a class="lgcookieslaw_btn" {if isset($cms_target) && $cms_target} target="_blank" {/if} href="{$cms_link|escape:'quotes':'UTF-8'}" >
						{stripslashes($button2|escape:'quotes':'UTF-8')}
					</a>
				</div>
			{/if}
			<div class="lgcookieslaw_message">{if version_compare($smarty.const._PS_VERSION_,'1.7.0','>=')}{stripslashes($cookie_message|escape:'quotes':'UTF-8') nofilter}{* HTML, cannot escape*}{else}{stripslashes($cookie_message|escape:'quotes':'UTF-8')}{/if}</div>
			{if $buttons_position == 5}
				<div class="lgcookieslaw_button_container">
					<div>
						<input name="aceptocookies" class="lgcookieslaw_btn" type="submit" value="{stripslashes($button1|escape:'quotes':'UTF-8')}" >
					</div>
					<div>
						<a class="lgcookieslaw_btn" {if isset($cms_target) && $cms_target} target="_blank" {/if} href="{$cms_link|escape:'quotes':'UTF-8'}" >
							{stripslashes($button2|escape:'quotes':'UTF-8')}
						</a>
					</div>
				</div>
			{/if}
		</div>
		{if $buttons_position == 3 }
			<input name="aceptocookies" class="lgcookieslaw_btn" type="submit" value="{stripslashes($button1|escape:'quotes':'UTF-8')}" >
			<a class="lgcookieslaw_btn" {if isset($cms_target) && $cms_target} target="_blank" {/if} href="{$cms_link|escape:'quotes':'UTF-8'}" >
				{stripslashes($button2|escape:'quotes':'UTF-8')}
			</a>
		{/if}
		{if $show_close}
			<div class="lgcookieslaw_btn-close">
				<img src="{$path_module|escape:'html':'UTF-8'}/views/img/close.png" alt="close" class="lgcookieslaw_close_banner_btn" onclick="closeinfo();">
			</div>
		{/if}
	</form>
</div>
