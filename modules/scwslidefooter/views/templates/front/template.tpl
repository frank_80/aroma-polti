{*
*  @author    Miguel Costa for emotionLoop
*  @copyright emotionLoop
*}
{$content|escape:nofilter}
<script type="text/javascript">

$(document).ready(function(){
	
	$('#slideFooter .owl-carousel').owlCarousel({
	    loop:true,
	    margin:0,
	    nav:true,
	    dots:false,
	    autoplay:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	})

})
</script>