<?php

class Product extends ProductCore
{

    public static function getProductProperties($id_lang, $row, Context $context = null)
    {
        if (!$row['id_product']) {
            return false;
        }

        if ($context == null) {
            $context = Context::getContext();
        }

        $id_product_attribute = $row['id_product_attribute'] = (!empty($row['id_product_attribute']) ? (int)$row['id_product_attribute'] : null);

        // Product::getDefaultAttribute is only called if id_product_attribute is missing from the SQL query at the origin of it:
        // consider adding it in order to avoid unnecessary queries
        $row['allow_oosp'] = Product::isAvailableWhenOutOfStock($row['out_of_stock']);
        if (Combination::isFeatureActive() && $id_product_attribute === null
            && ((isset($row['cache_default_attribute']) && ($ipa_default = $row['cache_default_attribute']) !== null)
                || ($ipa_default = Product::getDefaultAttribute($row['id_product'], !$row['allow_oosp'])))) {
            $id_product_attribute = $row['id_product_attribute'] = $ipa_default;
        }
        if (!Combination::isFeatureActive() || !isset($row['id_product_attribute'])) {
            $id_product_attribute = $row['id_product_attribute'] = 0;
        }

        // Tax
        $usetax = Tax::excludeTaxeOption();

        $cache_key = $row['id_product'].'-'.$id_product_attribute.'-'.$id_lang.'-'.(int)$usetax;
        if (isset($row['id_product_pack'])) {
            $cache_key .= '-pack'.$row['id_product_pack'];
        }

        if (isset(self::$producPropertiesCache[$cache_key])) {
            return array_merge($row, self::$producPropertiesCache[$cache_key]);
        }

        // Datas
        $row['category'] = Category::getLinkRewrite((int)$row['id_category_default'], (int)$id_lang);
        $row['link'] = $context->link->getProductLink((int)$row['id_product'], $row['link_rewrite'], $row['category'], $row['ean13']);

        $row['attribute_price'] = 0;
        if ($id_product_attribute) {
            $row['attribute_price'] = (float)Product::getProductAttributePrice($id_product_attribute);
        }

        $row['price_tax_exc'] = Product::getPriceStatic(
            (int)$row['id_product'],
            false,
            $id_product_attribute,
            (self::$_taxCalculationMethod == PS_TAX_EXC ? 2 : 6)
        );

        if (self::$_taxCalculationMethod == PS_TAX_EXC) {
            $row['price_tax_exc'] = Tools::ps_round($row['price_tax_exc'], 2);
            $row['price'] = Product::getPriceStatic(
                (int)$row['id_product'],
                true,
                $id_product_attribute,
                6
            );
            $row['price_without_reduction'] = Product::getPriceStatic(
                (int)$row['id_product'],
                false,
                $id_product_attribute,
                2,
                null,
                false,
                false
            );
        } else {
            $row['price'] = Tools::ps_round(
                Product::getPriceStatic(
                    (int)$row['id_product'],
                    true,
                    $id_product_attribute,
                    6
                ),
                (int)Configuration::get('PS_PRICE_DISPLAY_PRECISION')
            );
            $row['price_without_reduction'] = Product::getPriceStatic(
                (int)$row['id_product'],
                true,
                $id_product_attribute,
                6,
                null,
                false,
                false
            );
        }

        $row['reduction'] = Product::getPriceStatic(
            (int)$row['id_product'],
            (bool)$usetax,
            $id_product_attribute,
            6,
            null,
            true,
            true,
            1,
            true,
            null,
            null,
            null,
            $specific_prices
        );

        $row['specific_prices'] = $specific_prices;

        $row['quantity'] = Product::getQuantity(
            (int)$row['id_product'],
            0,
            isset($row['cache_is_pack']) ? $row['cache_is_pack'] : null
        );

        $row['quantity_all_versions'] = $row['quantity'];

        if ($row['id_product_attribute']) {
            $row['quantity'] = Product::getQuantity(
                (int)$row['id_product'],
                $id_product_attribute,
                isset($row['cache_is_pack']) ? $row['cache_is_pack'] : null
            );
        }

        $row['id_image'] = Product::defineProductImage($row, $id_lang);
        $row['features'] = Product::getFrontFeaturesStatic((int)$id_lang, $row['id_product']);

        $row['attachments'] = array();
        if (!isset($row['cache_has_attachments']) || $row['cache_has_attachments']) {
            $row['attachments'] = Product::getAttachmentsStatic((int)$id_lang, $row['id_product']);
        }

        $row['virtual'] = ((!isset($row['is_virtual']) || $row['is_virtual']) ? 1 : 0);

        // Pack management
        $row['pack'] = (!isset($row['cache_is_pack']) ? Pack::isPack($row['id_product']) : (int)$row['cache_is_pack']);
        $row['packItems'] = $row['pack'] ? Pack::getItemTable($row['id_product'], $id_lang) : array();
        $row['nopackprice'] = $row['pack'] ? Pack::noPackPrice($row['id_product']) : 0;
        if ($row['pack'] && !Pack::isInStock($row['id_product'])) {
            $row['quantity'] = 0;
        }

        $row['customization_required'] = false;
        if (isset($row['customizable']) && $row['customizable'] && Customization::isFeatureActive()) {
            if (count(Product::getRequiredCustomizableFieldsStatic((int)$row['id_product']))) {
                $row['customization_required'] = true;
            }
        }

        $p = new Product(null, true, null);
        $row['attributes_groups'] = $attribute_groups = $p->getAttributeGroups($row['id_product'], (int)$id_lang);

        $row['combs'] = $p->getCombinations($row['id_product'], (int)$id_lang, $attribute_groups);

        $row = Product::getTaxesInformations($row, $context);
        self::$producPropertiesCache[$cache_key] = $row;
        return self::$producPropertiesCache[$cache_key];
    }

    public function getAttributesByCombination($id_product, $id_group, $id_product_attribute, $id_lang) {
        $sql = "SELECT a.id_attribute, al.name AS attribute, ag.id_attribute_group, agl.name AS attribute_group";
        $sql.= " FROM "._DB_PREFIX_."attribute_group ag";
        $sql.= " JOIN `"._DB_PREFIX_."attribute_group_lang` agl ON agl.`id_attribute_group` = ag.`id_attribute_group`";
        $sql.= " JOIN "._DB_PREFIX_."attribute a ON a.`id_attribute_group` = ag.id_attribute_group";
        $sql.= " JOIN "._DB_PREFIX_."attribute_lang al ON al.`id_attribute` = a.`id_attribute`";
        $sql.= " JOIN `"._DB_PREFIX_."product_attribute_combination` pac ON a.`id_attribute` = pac.`id_attribute`";
        $sql.= " JOIN "._DB_PREFIX_."product_attribute pa ON pa.`id_product_attribute` = pac.id_product_attribute";
        $sql.= " WHERE ag.`id_attribute_group` = $id_group";
        $sql.= " AND id_product = $id_product";
        $sql.= " AND al.id_lang = $id_lang";
        $sql.= " AND agl.`id_lang` = $id_lang";
        $sql.= " AND pac.id_product_attribute = $id_product_attribute";
        $sql.= " GROUP BY ag.`id_attribute_group`";
        $sql.= " LIMIT 1";

        $result = Db::getInstance()->executeS($sql);
        return $result ? $result[0] : null;
    }

    public function getAttributeGroups($id_product = null, $id_lang) {
        $sql = "SELECT DISTINCT(ag.id_attribute_group), agl.name, ag.id_attribute_group AS id";
        $sql.= " FROM "._DB_PREFIX_."attribute_group ag";
        $sql.= " INNER JOIN "._DB_PREFIX_."attribute_group_lang agl ON agl.`id_attribute_group` = ag.`id_attribute_group`";
        $sql.= " INNER JOIN "._DB_PREFIX_."attribute a ON a.`id_attribute_group` = ag.`id_attribute_group`";
        $sql.= " INNER JOIN "._DB_PREFIX_."product_attribute_combination pac ON pac.`id_attribute` = a.`id_attribute`";
        $sql.= " INNER JOIN "._DB_PREFIX_."product_attribute pa ON pac.`id_product_attribute` = pa.`id_product_attribute`";
        $sql.= " WHERE agl.id_lang = $id_lang";
        $sql.= $id_product ? " AND pa.id_product = '$id_product'" : "";
        $sql.= " ORDER BY ag.position ASC";

        return Db::getInstance()->executeS($sql);
    }

    /**
    * Get all available product attributes combinations
    *
    * @param integer $id_lang Language id
    * @return array Product attributes combinations
    */
    public function getCombinations($id_product, $id_lang, $groups) {
        $sql = "SELECT pa.`id_product_attribute` AS id, ROUND(price,2) AS price, pa.reference, sa.quantity as max_qty";
        $sql.= " FROM "._DB_PREFIX_."product_attribute pa";
        $sql.= " INNER JOIN "._DB_PREFIX_."stock_available sa ON sa.id_product_attribute = pa.id_product_attribute";
        $sql.= " WHERE pa.id_product = $id_product";
        $sql.= " ORDER BY pa.`id_product_attribute` ASC";
        $combinations = Db::getInstance()->executeS($sql);
                
        $this->id = $id_product;

        foreach($groups as $group) {
            foreach ($combinations as &$comb) {
                $comb['attributes'][$group['id']] = $this->getAttributesByCombination($id_product, $group['id'], $comb['id'], $id_lang);
                $comb['price'] = Product::getPriceStatic($id_product, true, $comb['id'], 2, null, false, false);
                $comb['price_with_reduction'] = Product::getPrice(true, $comb['id'], 2);
                $comb_id = $comb['id'];
                $comb_images = $this->getCombinationImages($id_lang);
                if(isset($comb_images[$comb_id])) {
                    $comb['id_image'] = $comb_images[$comb_id][0]['id_image'];
                }

                $comb['quantity'] = StockAvailable::getQuantityAvailableByProduct($id_product, $comb['id']);
            }
        }
        return $combinations;
    }
}