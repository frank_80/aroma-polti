<?php

class Sync
{
    private $now;
	private $order;
	private $dir_name;
    private $idLocation;
    private $filedate;
    private $company_type_id;
    private $customer_type_id;
    private $payments;

    /**
     * Prepare order data to CSV file for sync with Management System
     * @param  Order $order Created order
     * @return void  Create all files to sync with Management System
     */
    public function __construct($order)
    {
        $this->now = time();
        $this->nowDate = date('Y-m-d');

    	$this->order = $order;

    	$this->idLocation = "P4011";// Se the ID of the shop to distingue this Shop from another
        $this->dir_name = _PS_UPLOAD_DIR_."sync";
        
        $this->company_type_id = 'N';
        $this->customer_type_id = 'Y';

        $this->address_type_shipping = 'D01';
        $this->address_type_invoice = 'FAT';
        $this->order_type_id = 'I';

        $this->payments = [
            'bankwire' => 'BDF3', // Bonifico a 30 gg. d.f.
            'cheque' => 'CA', // Assegno
            'codfee' => 'CA', // Contrassegno
            'paypal' => 'PP', // Paypal
        ];

        $this->getOrderData();
        $this->getProductsCart();
        $this->getCustomerData();
        $this->getAddressData('id_address_delivery');
        $this->getAddressData('id_address_invoice');
        $this->getCartRules();
    }

    /**
     * Get the dat_add and transaction code from order payment (PayPal, X-Pay,...)
     * @return String Transaction code ID
     */
    private function getPaymentData()
    {
    	return OrderPayment::getByOrderId($this->order->id);
    }

    private function getCustomerForMago($id_customer, $field = 'mago2')
    {
        if($customer = new Customer($id_customer)) {
            if($id_customer > 2000)
                return $id_customer + 2000;
            else if($customer->$field)
                return $customer->$field;
            else
                return $id_customer + 2000;
        }
        else {
            return $id_customer + 2000;
        }
    }

    /**
     * Get the message writed by customer in his order
     * @return String Text message
     */
    private function getCartRules()
    {
        $order = $this->order;
        $cart = new Cart($order->id_cart);
        
        $i = 1;
        $servicesData = [];


        /* Discounts service */
        $cart_rules = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
        SELECT *
        FROM `'._DB_PREFIX_.'order_cart_rule` ocr
        WHERE ocr.`id_order` = '.(int)$order->id);

        if($cart_rules) {
            $discounts_codes = [];

            foreach ($cart_rules as $cr) {
                $cart_rule = new CartRule($cr['id_cart_rule']);
                $discounts_codes[] = $cart_rule->code;
            }

            $item = [
                'idWebOrder' => $order->id,
                'idWebOrderLine' => $i * 10,
                'idLocation' => $this->idLocation,
                'idWebCust' => $this->getCustomerForMago($order->id_customer, 'mago1'),
                'DiscCode' => 'S',
                'DiscDescr' => implode(' ', $discounts_codes),
                'Sign' => '-',
                'DiscAmount' => SyncHelper::convPrice($order->total_discounts_tax_incl),
                'Type' => '$',
            ];
            $servicesData[] = $item;
            $i++;
        }

        /* Pay at delivery service */
        if($codfee = $this->getCodfee($order)) {
            $item = [
                'idWebOrder' => $order->id,
                'idWebOrderLine' => $i * 10,
                'idLocation' => $this->idLocation,
                'idWebCust' => $this->getCustomerForMago($order->id_customer, 'mago1'),
                'DiscCode' => 'C',
                'DiscDescr' => 'Contrassegno',
                'Sign' => '+',
                'DiscAmount' => SyncHelper::convPrice($codfee),
                'Type' => '$',
            ];
            $servicesData[] = $item;
            $i++;
        }

        /* Shipping service */
        if($order->total_shipping_tax_incl > 0) {
            $shipping = $order->total_shipping_tax_incl;
            $carrier = $order->getShipping();
            if($codfee) {
                $shipping -= $codfee;
            }

            $item = [
                'idWebOrder' => $order->id,
                'idWebOrderLine' => $i * 10,
                'idLocation' => $this->idLocation,
                'idWebCust' => $this->getCustomerForMago($order->id_customer, 'mago1'),
                'DiscCode' => 'T',
                'DiscDescr' => count($carrier) ? $carrier[0]['carrier_name'] : 'Trasporto',
                'Sign' => '+',
                'DiscAmount' => $shipping ? SyncHelper::convPrice($shipping) : 0,
                'Type' => '$',
            ];
            $servicesData[] = $item;
            $i++;
        }

        $this->toMS('txt', $servicesData, 'service', $order, 'ordsad');
    }

    private function getCodfee($order)
    {
        if($codfee_price = (float)Db::getInstance()->getValue('SELECT codfee FROM `'._DB_PREFIX_.bqSQL('orders').'` WHERE `'.bqSQL('id_order').'` = '.(int)$order->id)) {
            return $codfee_price;
        }
        else if($order->module == 'codfee') {
            return (float)Db::getInstance()->getValue('SELECT fix FROM `'._DB_PREFIX_.bqSQL('codfee_configuration').'` ORDER BY id_codfee_configuration DESC');
        }
    }

    /**
     * Get the message writed by customer in his order
     * @return String Text message
     */
    private function getOrderMessage()
    {
    	$sql = 'SELECT message FROM `'._DB_PREFIX_.'message` WHERE `id_order` = \''.$this->order->id.'\'';
        $result = Db::getInstance()->getValue($sql);

        return $result;
    }

    /**
     * Create the file to sync Order Head with Management System
     * @return void Create a new file in upload directory
     */
    private function getOrderData()
    {
    	$order = $this->order;
    	$payment = $this->getPaymentData();

        $orderData = [
            'idWebOrder' => $order->id,
            'idLocation' => $this->idLocation,
            'idWebCust' => $this->getCustomerForMago($order->id_customer, 'mago1'),
            'OrderDate' => SyncHelper::convDate($order->date_add),
            'Payment' => isset($this->payments[$order->module]) ? $this->payments[$order->module] : $order->module, // Payments setted in __contruct()
            'DescrPayment' => $payment->transaction_id ? $payment->transaction_id : $order->payment,
            'DatePayment' => $payment->date_add ? SyncHelper::convDate($payment->date_add) : SyncHelper::convDate($this->nowDate),
            'idErpCust' => $this->getCustomerForMago($order->id_customer, 'mago2'),
            'idErpOrder' => $order->id,
            'AddressType' => $this->address_type_shipping,
            'ShipComment' => strtoupper($this->getOrderMessage()),
            'OrderType' => $this->order_type_id,
        ];

        PrestaShopLogger::addLog("Start export order for Mago with ID: {$order->id}", 1, null, 'order', $order->id, true);
        $this->toMS('txt', $orderData, 'order', $order, 'ordtes');
    }

    /**
     * Create the file to sync each Order product in the cart with Management System
     * @return void Create a new file in upload directory
     */
    private function getProductsCart()
    {
        $order = $this->order;

        $cart = new Cart($order->id_cart);
        $products_cart = $cart->getProducts();
        $currency = new Currency($cart->id_currency);
        $datas = [];

        $i = 0;
        foreach ($products_cart as $item) {
            $i++;

            $data = [
                'idWebOrder' => $order->id,
                'idWebOrderline' => $i * 10,
                'idLocation' => $this->idLocation,
                'idWebCust' => $this->getCustomerForMago($order->id_customer, 'mago1'),
                'PromoCod' => null,
                'PromoDescr' => null,
                'idItemCode' => $item['reference'],
                'OrderQty' => SyncHelper::convQuantity($item['cart_quantity']),
                'Currency' => $currency->iso_code,
                'UnitPrice' => SyncHelper::convPrice($item['price_with_reduction']),
                'UnitListPrice' => SyncHelper::convPrice($item['price_without_reduction']),
            ];

            $datas[] = $data;
        }
        $this->toMS('txt', $datas, 'order', $order, "ordrig");
    }

    private function getCorp($field = 'firstname', $address, $customer) {
        if ($address->company || $customer->company) {
            if ($field == 'lastname') {// For company, the name must be only in firstname
                return '';
            } else {
                return $address->company ? $address->company : $customer->company;
            }
        } else {
            return $address->$field ? $address->$field : $customer->$field;
        }
    }

    /**
     * Create the file to sync Addresses data used in $order with Management System
     * @param  string $fieldname Then name of the address field, for delivery or invoice
     * @return void              Create a new file in upload directory
     */
    private function getAddressData($fieldname = 'id_address_delivery')
    {
    	$order = $this->order;

        if($fieldname == 'id_address_delivery') {
            $filename = 'clides';
            $AddressType = $this->address_type_shipping;
        }
        else {
            $filename = 'clifat';
            $AddressType = $this->address_type_invoice;
        }

        $address = new Address($order->$fieldname);
        $province = new State((int)$address->id_state);
        $customer = new Customer($order->id_customer);
        $country = new Country($address->id_country);

        $addressData = [
            'idLocation' => $this->idLocation,
            'idWebCust' => $this->getCustomerForMago($address->id_customer, 'mago1'),
            'TypeCust' => $address->vat_number ? $this->company_type_id : $this->customer_type_id,
            'AddressType' => $AddressType,
            'Corp1_Surname' => $this->getCorp('lastname', $address, $customer),
            'Corp2_Name' => $this->getCorp('firstname', $address, $customer),
            'Address1' => strtoupper($address->address1),
            'Address2' => strtoupper($address->address2),
            'Address3' => null,
            'ZipCode' => $address->postcode,
            'City' => strtoupper($address->city),
            'Country' => strtoupper($province->iso_code), // Warning! It's province alias!
            'State' => null, // Not used for import by Mago
            'Nation' => strtoupper($country->iso_code),
            'Telephone' => $address->phone,
            'Telefax' => null,
            'Mobile' => $address->phone_mobile,
            'ContactSurname' => strtoupper($address->lastname),
            'ContactName' => strtoupper($address->firstname),
            'CustInvoiceNote' => strtoupper($customer->other),
            'idErpCust' => $this->getCustomerForMago($customer->id, 'mago2'),
            'idWebOrder' => $order->id
        ];
        $this->toMS('txt', $addressData, 'address', $order, $filename);
    }

    /**
     * Create the file to sync Customer account data with Management System
     * @return void         Create a new file in upload directory
     */
    private function getCustomerData()
    {
    	$order = $this->order;

        $address = new Address($order->id_address_invoice);
        $province = new State((int)$address->id_state);
        $customer = new Customer($order->id_customer);
        $country = new Country($address->id_country);

        $genders = [1 => 'M', 2 => 'F'];
        $gender = isset($genders[$customer->id_gender]) ? $genders[$customer->id_gender] : null;

        $comp = ($address->vat_number && $customer->company);

        $customerData = [
            'idLocation'     => $this->idLocation,
            'idWebCust'      => $this->getCustomerForMago($customer->id, 'mago1'),
            'TypeCust'       => $address->vat_number ? $this->company_type_id :  $this->customer_type_id,
            'Corp1_Surname'  => $this->getCorp('lastname', $address, $customer),
            'Corp2_Name'     => $this->getCorp('firstname', $address, $customer),
            'BirthDate'      => $customer->birthday && $customer->birthday != '0000-00-00' ? SyncHelper::convDate($customer->birthday) : 0,
            'Address1'       => strtoupper($address->address1),
            'Address2'       => strtoupper($address->address2),
            'Address3'       => null,
            'ZipCode'        => $address->postcode,
            'City'           => strtoupper($address->city),
            'Country'        => strtoupper($province->iso_code), // Warning! It's province alias!
            'State'          => null, // Not used for import by Mago
            'Nation'         => strtoupper($country->iso_code),
            'Telephone'      => $address->phone,
            'Telefax'        => null,
            'Mobile'         => $address->phone_mobile,
            'Fiscalcode'     => strtoupper($address->dni),
            'NIF'            => $address->vat_number,
            'CustType'       => 'CC',
            'Privacy1'       => 'Y',
            'Privacy2'       => 'Y',
            'Privacy3'       => 'N',
            'Privacy4'       => 'Y',
            'EMail'          => $customer->email,
            'CustNote'       => $customer->note,
            'idErpCust'      => $this->getCustomerForMago($customer->id, 'mago2'),
            'sex'            => strtoupper($gender),
            'fiscal_Flag'    => SyncHelper::isDniLite($address->dni) ? 'N' : 'Y', // 'Y' if not correct
            'NifFlag'        => SyncHelper::isVatNumber($address->vat_number) ? 'N' : 'Y', // 'Y' if not correct
            'ActivationDate' => $customer->date_add, // With time format
            'ContactSurname' => $address->lastname,
            'ContactName'    => $address->firstname,
            'idWebOrder'     => (int)$order->id
        ];
        $this->toMS('txt', $customerData, 'customer', $order, "cliana");
    }

    private function getFileTimestamp()
    {
        $timestamp = $this->now;
        if(strlen($timestamp) < 14) {
            while (strlen($timestamp) < 14) {
                $timestamp = '0'.$timestamp;
            }
        }
        return $timestamp;
    }

    private function getValue($value)
    {
        if(is_int($value) && $value == 0) {
            $result = '"0"';
        } else {
            $value = htmlentities(trim($value));
            $result = $value ? "\"{$value}\"" : '" "';
        }
        return $result;
    }

    private function getRowData($data)
    {
        $data = array_values($data);
        $results = [];
        $result = '';
        $divisor = '|';

        foreach($data as $item) {
            $results[] = $this->getValue($item);

            $result = implode($divisor, $results);
        }
        return $result;
    }

    /**
     * Convert data and create a file for Management System
     * @param  string $format       Xml, Json or Txt
     * @param  array  $data         Data object
     * @param  string $rootNodeName Root tag node
     * @param  string $path         Destination directory local path
     * @param  string $filename     The name of new file
     * @return void                 Create a new file in upload directory
     */
    private function toMS($format = 'xml', array $data, $rootNodeName, $order, $filename)
    {
        $result = '';

        switch($format) {
            case 'xml':
                $xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");
     
                foreach($data as $key => $value) {
                    // replace anything not alpha numeric
                    $key = preg_replace('/[^a-z]/i', '', $key);
         
                    // if there is another array found recrusively call this function
                    if (is_array($value)) {
                        $node = $xml->addChild($key);
                        // recrusive call.
                        ArrayToXML::toXml($value, $rootNodeName, $node);
                    }
                    else {
                        // add single node.
                        $value = htmlentities($value);
                        $xml->addChild($key, $value);
                    }
                }

                $result = $xml->asXML();

                break;

            case 'json':
                $result = json_encode($data, JSON_PRETTY_PRINT);

                break;

            default:
                // Remove the hash keys
                if(in_array($filename, ['ordrig', 'ordsad'])) {
                    $results = [];
                    foreach ($data as $item) {
                        $results[] = $this->getRowData($item);
                    }
                    $result = implode("\n", $results);
                } else {
                    $result = $this->getRowData($data);
                }
        }

        if($result) {
            $type = $filename;
            $filedate = SyncHelper::convDate($this->nowDate);
            $timestamp = $this->getFileTimestamp();
            $filename = "Q_{$filename}_{$filedate}_{$timestamp}.{$format}";

            $fp = fopen("{$this->dir_name}/{$filename}", 'w');
            fwrite($fp, $result);
            fclose($fp);

            $escaped_result = str_replace('"', '', $result);
            PrestaShopLogger::addLog("Uploaded file for Mago: {$filename} with data: {$escaped_result}", 1, null, $type, $order->id, true);

            if(getenv('APP_ENV') != 'dev') {
                SyncHelper::upload("{$filename}");
            }
        }
    }
}