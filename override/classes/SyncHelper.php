<?php

abstract class SyncHelper
{

	public function upload($file_originale)
	{
		$server = 'aromapolti.it';
		$username = 'aromapoltiimport';
		$password = 'iBab!030';

		$id_connessione = ftp_connect($server);
		 
		$login = ftp_login($id_connessione, $username, $password);

		$file_destinazione = $file_originale;
		$file_originale = _PS_UPLOAD_DIR_."sync/{$file_originale}";

        $path = getenv('APP_ENV') ? getenv('APP_ENV') : 'prod'; //default dir prod
		 
		ftp_chdir($id_connessione, '/'.$path.'/');
		ftp_put($id_connessione, $file_destinazione, $file_originale, FTP_ASCII);
		ftp_close($id_connessione);

		unlink($file_originale);
	}

    /**
     * Convert timestamp as 2017-02-23 10:25:59 in 20170223
     * @param  String $date Mysql date format
     * @return String       Converted date without time and divisors
     */
    public function convDate($date)
    {
    	$date = explode(' ', $date)[0];
    	return str_replace('-', '', $date);
    }

    /**
     * Convert a quantity value for Management System format
     * @param  int $quantity Quantity of products
     * @return int Converted quantity
     */
    public function convQuantity($quantity)
    {
    	return $quantity * 100;
    }

    /**
     * Convert a percent value for Management System format ex.: 12% => 1200
     * @param  int $quantity Quantity of products
     * @return int Converted quantity
     */
    public function convPercent($percent)
    {
    	return round($percent, 2) * 100;
    }

    /**
     * Convert a price value for Management System format
     * @param  int $price Price of products or discounts
     * @return int Converted price
     */
    public function convPrice($price)
    {
    	return round($price, 2) * 100000;
    }

    /**
	* Check for Dni validity
	*
	* @param string $dni to validate
	* @return int
	*/
	public function isDniLite($dni)
	{		
		$dni = strtoupper($dni);
		if (!preg_match('/^[a-z]{6}[0-9]{2}[a-z][0-9]{2}[a-z][0-9]{3}[a-z]$/i', $dni)) 
			return false;
		
		for ($i=0;$i<9;$i++)
			$char[$i] = substr($dni, $i, 1);
		// 12345678T
		if (preg_match('/(^[0-9]{8}[A-Z]{1}$)/', $dni))
			if ($char[8] != substr('TRWAGMYFPDXBNJZSQVHLCKE', substr($dni, 0, 8) % 23, 1))
				return false;
		
		$sum = $char[2] + $char[4] + $char[6];
		for ($i = 1; $i < 8; $i += 2)
			$sum += substr((2 * $char[$i]),0,1) + substr((2 * $char[$i]),1,1);
		
		$n = 10 - substr($sum, strlen($sum) - 1, 1);
		
		/*if (preg_match('/^[KLM]{1}/', $dni))
			if ($char[8] != chr(64 + $n))
				return false;*/
		
		if (preg_match('/^[ABCDEFGHJNPQRSUVW]{1}/', $dni))
			if ($char[8] != chr(64 + $n) || $char[8] == substr($n, strlen($n) - 1, 1))
				return false;
		
		if (preg_match('/^[T]{1}/', $dni))
			if ($char[8] != preg_match('/^[T]{1}[A-Z0-9]{8}$/', $dni))
				return false;
		
		if (preg_match('/^[XYZ]{1}/', $dni))
			if ($char[8] != substr('TRWAGMYFPDXBNJZSQVHLCKE', substr(str_replace(array('X','Y','Z'), array('0','1','2'), $dni), 0, 8) % 23, 1))
				return false;
		
		return true;
	}

	/**
	 * Validate a Vat Number
	 * @param  String  $vat Vat Number
	 * @return boolean TRUE if is a valid Vat Number (format)
	 */
	public function isVatNumber($vat)
	{
		return preg_match("/^(IT){0,1}[0-9]{11}$/i", trim($vat));
	}
}