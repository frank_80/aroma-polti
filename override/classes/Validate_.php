<?php

class Validate extends ValidateCore
{

    /**
	* Check for Dni validity
	*
	* @param string $dni to validate
	* @return int
	* @deprecated
	*/
	public static function isDniLite($dni)
	{
		/*
		Return code:
		1 : It's Ok
		0 : Bad format for DNI
		-1 : DNI duplicate
		-2 : NIF error
		-3 : CIF error
		-4 : NIE error
		*/
		//Tools::displayAsDeprecated();
		
		if (!$dni)
			return 1;
		
		$dni = strtoupper($dni);
		if (!preg_match('/^[a-z]{6}[0-9]{2}[a-z][0-9]{2}[a-z][0-9]{3}[a-z]$/i', $dni)) 
			return 0;
		
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
		SELECT `id_address`
		FROM `'._DB_PREFIX_.'address` 
		WHERE `dni` = \''.pSQL($dni).'\'');
		if ($result)
			return -1;
		
		for ($i=0;$i<9;$i++)
			$char[$i] = substr($dni, $i, 1);
		// 12345678T
		if (preg_match('/(^[0-9]{8}[A-Z]{1}$)/', $dni))
			if ($char[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr($dni, 0, 8) % 23, 1))
				return 1;
			else
				return -2;
		
		$sum = $char[2] + $char[4] + $char[6];
		for ($i = 1; $i < 8; $i += 2)
			$sum += substr((2 * $char[$i]),0,1) + substr((2 * $char[$i]),1,1);
		
		$n = 10 - substr($sum, strlen($sum) - 1, 1);
		
		if (preg_match('/^[KLM]{1}/', $dni))
			if ($char[8] == chr(64 + $n))
				return 1;
			else
	 			return -2;
		
		if (preg_match('/^[ABCDEFGHJNPQRSUVW]{1}/', $dni))
			if ($char[8] == chr(64 + $n) || $char[8] == substr($n, strlen($n) - 1, 1))
				return 1;
			else
				return -3;
		
		if (preg_match('/^[T]{1}/', $dni))
			if ($char[8] == preg_match('/^[T]{1}[A-Z0-9]{8}$/', $dni))
				return 1;
			else
				return -4;
		
		if (preg_match('/^[XYZ]{1}/', $dni))
			if ($char[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr(str_replace(array('X','Y','Z'), array('0','1','2'), $dni), 0, 8) % 23, 1))
				return 1;
			else
				return -4;
		
		return 0;
	}
}