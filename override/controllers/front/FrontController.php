<?php

class FrontController extends FrontControllerCore
{
    public function initHeader()
    {
        /** @see P3P Policies (http://www.w3.org/TR/2002/REC-P3P-20020416/#compact_policies) */
        header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');
        header('Powered-By: PrestaShop');

        // Hooks are voluntary out the initialize array (need those variables already assigned)
        $this->context->smarty->assign(array(
            'time'                  => time(),
            'img_update_time'       => Configuration::get('PS_IMG_UPDATE_TIME'),
            'static_token'          => Tools::getToken(false),
            'token'                 => Tools::getToken(),
            'priceDisplayPrecision' => _PS_PRICE_DISPLAY_PRECISION_,
            'content_only'          => (int)Tools::getValue('content_only'),
        ));

        $this->context->smarty->assign($this->initLogoAndFavicon());

        // Customizatio
        if ($orders = Order::getCustomerOrders($this->context->customer->id)) {
            $this->context->smarty->assign('last_id_order', reset($orders)['id_order']);
        }
    }
}