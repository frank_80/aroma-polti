<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{hpp}default-bootstrap>column15_03c2e7e41ffc181a4e84080b4710e81e'] = 'Novità';
$_MODULE['<{hpp}default-bootstrap>column15_d3da97e2d9aee5c8fbe03156ad051c99'] = 'More';
$_MODULE['<{hpp}default-bootstrap>column15_4351cfebe4b61d8aa5efa1d020710005'] = 'Vedi';
$_MODULE['<{hpp}default-bootstrap>column15_2d0f6b8300be19cf35e89e66f0677f95'] = 'Acquista';
$_MODULE['<{hpp}default-bootstrap>column15_1a8c0228da8fb16885881e6c97b96f08'] = 'Nessun prodotto al momento.';
$_MODULE['<{hpp}default-bootstrap>column16_1a8c0228da8fb16885881e6c97b96f08'] = 'Nessun prodotto al momento.';
$_MODULE['<{hpp}default-bootstrap>internal-tabs_03c2e7e41ffc181a4e84080b4710e81e'] = 'Novità';
$_MODULE['<{hpp}default-bootstrap>internal-tabs_d3da97e2d9aee5c8fbe03156ad051c99'] = 'Dettagli';
$_MODULE['<{hpp}default-bootstrap>internal-tabs_4351cfebe4b61d8aa5efa1d020710005'] = 'Vedi';
$_MODULE['<{hpp}default-bootstrap>internal-tabs_2d0f6b8300be19cf35e89e66f0677f95'] = 'Acquista';
$_MODULE['<{hpp}default-bootstrap>internal-tabs_1a8c0228da8fb16885881e6c97b96f08'] = 'Nessun prodotto al momento';
$_MODULE['<{hpp}default-bootstrap>products_03c2e7e41ffc181a4e84080b4710e81e'] = 'Novità';
$_MODULE['<{hpp}default-bootstrap>products_d3da97e2d9aee5c8fbe03156ad051c99'] = 'Dettagli';
$_MODULE['<{hpp}default-bootstrap>products_4351cfebe4b61d8aa5efa1d020710005'] = 'Vedi';
$_MODULE['<{hpp}default-bootstrap>products_2d0f6b8300be19cf35e89e66f0677f95'] = 'Acquista';
$_MODULE['<{hpp}default-bootstrap>products_1a8c0228da8fb16885881e6c97b96f08'] = 'Nessun prodotto al momento';
$_MODULE['<{hpp}default-bootstrap>products16_1a8c0228da8fb16885881e6c97b96f08'] = 'Nessun prodotto al momento';
$_MODULE['<{hpp}default-bootstrap>tabcontainer16_1a8c0228da8fb16885881e6c97b96f08'] = 'Nessun prodotto al momento';
